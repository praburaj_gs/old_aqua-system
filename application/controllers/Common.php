<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		if(!$this->fb_rest->isloggedin()){
			redirect("/");
		}
	}
	
	public function index()
	{
		echo "this is common controller";
	}
	
	public function check_duplicate(){
		$search = $this->input->get_post("search", true);
		$fld_name = $this->input->get_post("fld_name", true);
		$table_name = $this->input->get_post("table_name", true);
		$rkey = $this->input->get_post("rkey", true);
		
		$rst = $this->fb_rest->check_duplicate($table_name,$fld_name,$search,$rkey);
		echo json_encode($rst);
		exit;
	}
	
	public function change_lang(){
		$lang = $this->input->get_post("lang");
		$ruri = $this->input->get_post("ruri");
		$ruri = !empty($ruri)? "/".$ruri : "/dashboard";
		$clang = $this->config->item('language', 'fb_boodskap');
		$alang = array_keys($clang);
		$lang = (in_array($lang, $alang)) ? $lang : "en";
		$this->session->set_userdata("lang", $lang);
		redirect($ruri);
	}
	
	function getActivities(){
	 $ci =& get_instance();
	 $page_no = $this->input->get("page");
	 $per_page=10;	 
	 $from = ($page_no <= 1) ? "0" : ( $page_no - 1 ) ;
	 $from = ($from * $per_page);
	 
	  $table_name = "activities";
	  $result = $ci->fb_rest->lod_more($table_name,$from,$per_page);	

	   $activities = $result['result_set'];
	  if(!empty($activities)) {
		foreach($activities as $activity): 
		$_source = $activity['_source'];
		$activityArr= array();
		$activityArr= unserialize($_source['activity_ref']);
		//print_r($activityArr);
		?>
			<div class="feed-item">
			  <div class="date">
				<?=  date("d F Y H:i:s", $_source['createdtime']) ?>
			  </div>
			  <div class="text">
				<?= $activityArr['msg']; ?>
			  </div>
			</div>
			<?php endforeach; 
		}
		
	  //$activity = $result['result_set'];		
	  //return $activity;

	}
	
	public function common_js(){
		$this->load->library('parser');
		$params["site_url"] = site_url("/");
		$ajs_lang = fb_jslang();
		$params["site_err_lang"] = json_encode($ajs_lang);
		$cjs = $this->parser->parse('include/common_js', $params, true);
		$this->output
			->set_content_type('application/javascript')
			->set_output($cjs);
	}
	
	
}
