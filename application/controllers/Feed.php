<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feed extends MY_Controller {
	
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('date');
		$this->table="feeds";
	}
	
	public function index()
	{
		if($this->fb_rest->isloggedin()){
  		    /* $this->load->view('include/header');
			$this->load->view('include/left_menu');
			$this->load->view('layout/feedlist_content');
			$this->load->view('include/footer'); */
			$data = array();
			$page_no = $this->uri->segment('2');
			$per_page = $this->input->get_post("no_items", true);
			$search = $this->input->get_post("search", true);
			$sort_fld = $this->input->get_post("sort_fld", true);
			$sort_dir = $this->input->get_post("sort_dir", true);
			$page_burl = site_url("/feed");
			$this->table="feeds";
			$table_name = $this->table;
			$params =  array("page_no" => $page_no, "per_page" => $per_page, "uri_segment" => "2",
			"search" => $search, "sort_fld" => $sort_fld, "sort_dir" => $sort_dir, "page_burl" => $page_burl, "table_name" => $table_name);
			
			$data["sort_fld"] = $sort_fld;
			$data["sort_dir"] = $sort_dir;
			$data["search"] = $search;
			$data["per_page"] = $per_page;
			
			$sort_columns = array("feedname", "feed_type", "size");
			//echo fb_text("feed_stock_distribution_list"); 
			$hstr = array("feedname" =>fb_text("feed_name"), "feed_type" => fb_text("feed_type"), "size" => fb_text("size"),
			"action" => fb_text("action")
			);
			
			$theader = "";
			
			foreach($hstr as $hk => $hv)
			{
				if(in_array($hk, $sort_columns)){
					$cdir = ($hk == $sort_fld) ? (($sort_dir=="asc") ? "desc" : "asc" ) : "asc";
					$pstr = (!empty($per_page)) ? $per_page : "10";
					$srt_params = array("sort_fld" => $hk, "sort_dir" => $cdir, "no_items" => $pstr, "search" => $search);
					$srt_str = http_build_query($srt_params);
					$srt_url = site_url("/feed?$srt_str");
					$cdir_icon = "";
					if(!empty($sort_fld)){
						$cdir_icon = ($hk == $sort_fld) ? 
						(($sort_dir=="asc") ? "&nbsp;<i class=\"fa fa-sort-asc\"></i>" : "&nbsp;<i class=\"fa fa-sort-desc\"></i>" ) : "";
					}
					$thstr = $hv.$cdir_icon;
					$thtml = "<th><a href='$srt_url'>$thstr</a></th>";
					$theader .= $thtml."\n";
				}else{
					$theader .= "<th>$hv</th>\n";
				}
			}
			
			$data["theader"] = $theader;
			$msg  = $this->fb_rest->list_record($params);
		
		
  		    $this->load->view('include/header');
			$this->load->view('include/left_menu');
			
			//fb_pr($msg);
			if($msg["status"] == "success")
			{
				$data["page_links"] = $msg["page_links"];
				$data["result_set"] = $msg["result_set"];
				$this->load->view("layout/feedlist_content", $data);
			}else{
				//$this->load->view("error", $data);
			}
			$this->load->view('include/footer');
			
		}else{
			redirect('/login');
		}
	}
	
	public function add_record(){
		$form_data = $this->input->post();
		
		$feedname = $this->input->post("feedname");
		$feed_type = $this->input->post("feed_type");
		$size = $this->input->post("size");

			$table_name = "feeds";
			$idata = array("feedname" => $feedname, 
			"size" => $size, 
			"feed_type" => $feed_type,
			"createdtime" => now(), 
			"updatedtime" => now());
			
			$result = $this->fb_rest->create_record($table_name, $idata);
			 //fb_pr($result);
		if($result['status']=="success"){
			$this->session->set_flashdata('success',fb_text("success"));
			redirect('/feed');
		}else{
			$this->session->set_flashdata('failed',fb_text("failed"));
			redirect('/feed');
		}
		
	}
	
	public function list_record(){
		$table_name = "feeds";
		$this->fb_rest->test_list_record($table_name);
	}

	function editFeed(){
		$data = array();
		$table_name = "feeds";
		$table_name=$this->table;
		$rkey = $this->input->post("rid");
		$record= $this->fb_rest->get_record($table_name, $rkey);
		if($record["status"] == "success")
		 {	
			$data['record'] = $record["result_set"];
			$data['rkey'] = $rkey;
			$this->load->view("layout/edit/feed_edit_content", $data);
		 }
		
	}
	function update(){
		$table_name = "feeds";
		$table_name=$this->table;
		$form_data = $this->input->post();
		$form_data['updatedtime']=now();	
		$feedname = $this->input->post("feedname");
		$feed_type = $this->input->post("feed_type");
		$size = $this->input->post("size");

			
			$idata = array("feedname" => $feedname, 
			"size" => $size, 
			"feed_type" => $feed_type,
			"updatedtime" => now());
		$rkey = $this->input->post("rkey");
		//$result = $this->fb_rest->update_record($table_name,$form_data,$rkey);
//		print_r($result);
		$oresult = $this->fb_rest->get_record($table_name, $rkey);
		$orecord = $oresult["result_set"];
		$result = $this->fb_rest->update_record($table_name, $idata, $rkey);
		if($result['status']=="success"){
		$this->fb_rest->update_others($table_name, $orecord, $idata, $rkey);
		$this->session->set_flashdata('update_success',fb_text("update_success"));
		redirect('/feed');
		}else{
		$this->session->set_flashdata('update_failed', fb_text("update_failed"));
		redirect('/feed');
		}
	}
	public function delete(){
		$table_name=$this->table;
		$rkey = $this->input->post("rid");	
		$result= $this->fb_rest->delete_record($table_name, $rkey);
		
		if($result['status']=="success"){
			$this->session->set_flashdata('delete_success',fb_text("delete_success"));
			redirect('/feed');
		}else{
			$this->session->set_flashdata('delete_failed',fb_text("delete_failed"));
			redirect('/feed');
		}
	}
	
	public function feedStock(){
		
		if($this->fb_rest->isloggedin()){

			$data = array();
			$page_no = $this->uri->segment('2');
			$per_page = $this->input->get_post("no_items", true);
			$search = $this->input->get_post("search", true);
			$sort_fld = $this->input->get_post("sort_fld", true);
			$sort_dir = $this->input->get_post("sort_dir", true);
			$page_burl = site_url("/feedStock");
			$this->table="feed_stock";
			$table_name = $this->table;
			$params =  array("page_no" => $page_no, "per_page" => $per_page, "uri_segment" => "2",
			"search" => $search, "sort_fld" => $sort_fld, "sort_dir" => $sort_dir, "page_burl" => $page_burl, "table_name" => $table_name);
			
			$data["sort_fld"] = $sort_fld;
			$data["sort_dir"] = $sort_dir;
			$data["search"] = $search;
			$data["per_page"] = $per_page;
			
			$sort_columns = array("feed_name", "weight", "date");
			
			$hstr = array("feed_name" => fb_text("feed_name"), "stocked_weight" => fb_text("stocked_weight"),"current_weight" => fb_text("current_weight"), "date" => fb_text("date"),"action" => fb_text("action")
			);
			
			$theader = "";
			
			foreach($hstr as $hk => $hv)
			{
				if(in_array($hk, $sort_columns)){
					$cdir = ($hk == $sort_fld) ? (($sort_dir=="asc") ? "desc" : "asc" ) : "asc";
					$pstr = (!empty($per_page)) ? $per_page : "10";
					$srt_params = array("sort_fld" => $hk, "sort_dir" => $cdir, "no_items" => $pstr, "search" => $search);
					$srt_str = http_build_query($srt_params);
					$srt_url = site_url("/feed/feedStock?$srt_str");
					$cdir_icon = "";
					if(!empty($sort_fld)){
						$cdir_icon = ($hk == $sort_fld) ? 
						(($sort_dir=="asc") ? "&nbsp;<i class=\"fa fa-sort-asc\"></i>" : "&nbsp;<i class=\"fa fa-sort-desc\"></i>" ) : "";
					}
					$thstr = $hv.$cdir_icon;
					$thtml = "<th><a href='$srt_url'>$thstr</a></th>";
					$theader .= $thtml."\n";
				}else{
					$theader .= "<th>$hv</th>\n";
				}
			}
			
			$data["theader"] = $theader;
			$msg  = $this->fb_rest->list_record($params);
		
		
  		    $this->load->view('include/header');
			$this->load->view('include/left_menu');
			
			//fb_pr($msg);
			if($msg["status"] == "success")
			{
				$data["page_links"] = $msg["page_links"];
				$data["result_set"] = $msg["result_set"];
				$this->load->view("layout/feedstock_content", $data);
			}else{
				//$this->load->view("error", $data);
			}
			$this->load->view('include/footer');
			
		}else{
			redirect('/login');
		}		
		
	}
	
	public function addFeedstock(){
		$form_data = $this->input->post();
		$form_data['createdtime']=now();
		$form_data['updatedtime']=now();	
		$form_data['current_weight'] = $this->input->post("stocked_weight");
		$form_date = $this->input->post("date");
		$form_data['stocked_date'] = fb_convert_time($form_date);

		$feedid = $form_data["feed_name"];
		$feeds = $this->fb_rest->get_record("feeds", $feedid);
		$form_data['feed_name'] = $feeds['result_set']['feedname'];
			//print_r($form_data); exit;
			$table_name = "feed_stock";
			$result = $this->fb_rest->create_record($table_name, $form_data);
			if($result['status']=="success"){
				$this->session->set_flashdata('success',fb_text("success"));
				redirect('feed/feedStock');
			}else{
				$this->session->set_flashdata('failed',fb_text("failed"));
				redirect('feed/feedStock');
			}
		
	}	
	
	function editfeedStock(){
		$data = array();
		$table_name="feed_stock";
		$rkey = $this->input->post("rid");
		$record= $this->fb_rest->get_record($table_name, $rkey);
		if($record["status"] == "success")
		 {	
		 	$data['record'] = $record["result_set"];
			$data['rkey'] = $rkey;
			$this->load->view("layout/edit/feedstock_edit_content", $data);
		 }
		
	}	
	
	public function deleteFeedstock(){
		$table_name = "feed_stock";
		$rkey = $this->input->post("rid");
		$orecord = $this->fb_rest->get_record($table_name, $rkey);
		$result= $this->fb_rest->delete_record($table_name, $rkey);
		
		if($result['status']=="success"){
			$this->session->set_flashdata('delete_success',fb_text("delete_success"));
			//$oresource = $orecord["result_set"];
			//$this->fb_rest->update_current_stock($oresource["pondname"], $oresource["species_type"], $oresource["count"], 'reduce');
			redirect('feed/feedStock');
		}else{
			$this->session->set_flashdata('delete_failed',fb_text("delete_failed"));
			redirect('feed/feedStock');
		}		
	}
	
	public function feedDistribution(){
		
		if($this->fb_rest->isloggedin()){

			$data = array();
			$page_no = $this->uri->segment('2');
			$per_page = $this->input->get_post("no_items", true);
			$search = $this->input->get_post("search", true);
			$sort_fld = $this->input->get_post("sort_fld", true);
			$sort_dir = $this->input->get_post("sort_dir", true);
			$page_burl = site_url("/feedDistribution");
			$this->table="feed_distribution";
			$table_name = $this->table;
			$params =  array("page_no" => $page_no, "per_page" => $per_page, "uri_segment" => "2",
			"search" => $search, "sort_fld" => $sort_fld, "sort_dir" => $sort_dir, "page_burl" => $page_burl, "table_name" => $table_name);
			
			$data["sort_fld"] = $sort_fld;
			$data["sort_dir"] = $sort_dir;
			$data["search"] = $search;
			$data["per_page"] = $per_page;
			
			$sort_columns = array("pond_name","feed_name", "weight", "date");
			
			$hstr = array("pond_name" => fb_text("pond_name"),"feed_name" => fb_text("feed_name"), "weight" => fb_text("weight"), "date" => fb_text("date"),
			"action" => fb_text("action"));
			
			$theader = "";
			
			foreach($hstr as $hk => $hv)
			{
				if(in_array($hk, $sort_columns)){
					$cdir = ($hk == $sort_fld) ? (($sort_dir=="asc") ? "desc" : "asc" ) : "asc";
					$pstr = (!empty($per_page)) ? $per_page : "10";
					$srt_params = array("sort_fld" => $hk, "sort_dir" => $cdir, "no_items" => $pstr, "search" => $search);
					$srt_str = http_build_query($srt_params);
					$srt_url = site_url("/feed?$srt_str");
					$cdir_icon = "";
					if(!empty($sort_fld)){
						$cdir_icon = ($hk == $sort_fld) ? 
						(($sort_dir=="asc") ? "&nbsp;<i class=\"fa fa-sort-asc\"></i>" : "&nbsp;<i class=\"fa fa-sort-desc\"></i>" ) : "";
					}
					$thstr = $hv.$cdir_icon;
					$thtml = "<th><a href='$srt_url'>$thstr</a></th>";
					$theader .= $thtml."\n";
				}else{
					$theader .= "<th>$hv</th>\n";
				}
			}
			
			$data["theader"] = $theader;
			$msg  = $this->fb_rest->list_record($params);
		
		
  		    $this->load->view('include/header');
			$this->load->view('include/left_menu');
			
			//fb_pr($msg);
			if($msg["status"] == "success")
			{
				$data["page_links"] = $msg["page_links"];
				$data["result_set"] = $msg["result_set"];
				$this->load->view("layout/feeddistribution_content", $data);
			}else{
				//$this->load->view("error", $data);
			}
			$this->load->view('include/footer');
			
		}else{
			redirect('/login');
		}			
		
	}
	
	public function addFeedDistribution(){
		
		
		$stock_id = $this->input->post("feed_stock_id");
		$pond_id = $this->input->post("pond_id");
		$weight = $this->input->post("weight");
		$form_data = $this->input->post();
		$form_data['createdtime']=now();
		$form_data['updatedtime']=now();	
		$form_data['date'] = fb_convert_time($this->input->post("date"));
			
		
		$stock = $this->fb_rest->get_record("feed_stock", $stock_id);
		$form_data['feed_name'] = $stock['result_set']['feed_name'];
		
		$pond = $this->fb_rest->get_record("ponds", $pond_id);
		$form_data['pond_name'] = $pond['result_set']['pondname'];
		//print_r($form_data); exit;		
		$table_name = "feed_distribution";
		$result = $this->fb_rest->create_record($table_name, $form_data);
		if($result['status']=="success"){
			$this->fb_rest->update_feed_stock($stock_id,$weight,$form_data['feed_name']);
			$this->session->set_flashdata('success',fb_text("success"));
			redirect('feed/feedDistribution');
		}else{
			$this->session->set_flashdata('failed',fb_text("failed"));
			redirect('feed/feedDistribution');
		}		
	}
	
	public function deleteFeedDistribution(){
		$table_name = "feed_distribution";
		$rkey = $this->input->post("rid");
		$orecord = $this->fb_rest->get_record($table_name, $rkey);
		$result= $this->fb_rest->delete_record($table_name, $rkey);
		
		if($result['status']=="success"){
			$this->session->set_flashdata('delete_success',fb_text("delete_success"));
			//$oresource = $orecord["result_set"];
			//$this->fb_rest->update_current_stock($oresource["pondname"], $oresource["species_type"], $oresource["count"], 'reduce');
			redirect('feed/feedDistribution');
		}else{
			$this->session->set_flashdata('delete_failed',fb_text("delete_failed"));
			redirect('feed/feedDistribution');
		}			
	}
	
   public function getcurrentFeed(){
	   $rkey = $this->input->post("rid");
	   $table_name="feed_stock";
	   $result = $this->fb_rest->get_record($table_name, $rkey);
	   echo json_encode($result,true);
   }
}
?>