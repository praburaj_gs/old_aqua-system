<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Distribution extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
  public function __construct(){
  parent::__construct();
	  $this->load->helper('date');
	   $this->table="distribution";
 }

	public function index()
	{
		if($this->fb_rest->isloggedin()){
		$data = array();
		$page_no = $this->uri->segment('2');
		$per_page = $this->input->get_post("per_page", true);
		$search = $this->input->get_post("search", true);
		$sort_fld = $this->input->get_post("sort_fld", true);
		$sort_dir = $this->input->get_post("sort_dir", true);
		$page_burl = site_url("/distribution");
		$table_name = $this->table;
		$params =  array("page_no" => $page_no, "per_page" => $per_page, "uri_segment" => "2",
		"search" => $search, "sort_fld" => $sort_fld, "sort_dir" => $sort_dir, "page_burl" => $page_burl, "table_name" => $table_name);
		//$params = array();
		  // or  $params = compact("page_no", "per_page", "search", "sort_fld", "sort_dir", "page_burl", "table_name");
		  
		$msg  = $this->fb_rest->list_record($params);
		
		
  		    $this->load->view('include/header');
			$this->load->view('include/left_menu');
			
			//fb_pr($msg);
			if($msg["status"] == "success")
			{
				$data["page_links"] = $msg["page_links"];
				$data["result_set"] = $msg["result_set"];
				$this->load->view("layout/distribution_content", $data);
			}else{
				//$this->load->view("error", $data);
			}
		
			$this->load->view('include/footer');
		}else{
			redirect('/login');
		}
		
	}
	
	function create(){
		$table_name=$this->table;
		$form_data = $this->input->post();
		$form_data['updatedtime'] = now();	
		$form_data['createdtime'] = now();
		$form_date = $this->input->post("date");
		$form_data['distribution_date'] = fb_convert_time($form_date);
		$date = explode('/', $form_date);
		$form_data['_month'] = $date[0];
		$form_data['_day']   = $date[1];
		$form_data['_year']  = $date[2];
		
		// From Pond id and name update
		$pid = $form_data["from_pond"]; // Actually get pond id		
		$crst = $this->fb_rest->get_record("ponds", $pid);
		$rst = $crst["result_set"];
		$pname = $rst["pondname"];
		$form_data["from_pond"] = $pname;
		$form_data["from_pond_id"] = $pid;
		
		// To Pond id and name update
		$pid = $form_data["to_pond"]; // Actually get pond id		
		$crst = $this->fb_rest->get_record("ponds", $pid);
		$rst = $crst["result_set"];
		$pname = $rst["pondname"];
		$form_data["to_pond"] = $pname;
		$form_data["to_pond_id"] = $pid;

		
		// Species type and Species ID update
		$sid = $form_data["species_type"]; // Actually get Species id
		$crst1 = $this->fb_rest->get_record("species", $sid);
		$rst1 = $crst1["result_set"];
		$stype = $rst1["species_type"];
		$form_data["species_type"] = $stype;
		$form_data["species_id"] = $sid;

		$result = $this->fb_rest->create_record($table_name,$form_data);
//		print_r($result);
		if($result['status']=="success"){
			$this->session->set_flashdata('success','Record Created');
			$this->fb_rest->update_current_stock($form_data["from_pond"], $form_data["species_type"], $form_data["count"], 'reduce', $pid, $sid);
			$this->fb_rest->update_current_stock($form_data["to_pond"], $form_data["species_type"], $form_data["count"], 'add', $pid, $sid);
			redirect('/distribution');
		}else{
			$this->session->set_flashdata('failed','Insert failed, Try Again!');
			redirect('/distribution');
		}
	}
	
	function delete(){
		$table_name=$this->table;
		$rkey = $this->input->post("rid");	
		$result= $this->fb_rest->delete_record($table_name, $rkey);
		
		if($result['status']=="success"){
			$this->session->set_flashdata('delete_success','Record Deleted');
			redirect('/distribution');
		}else{
			$this->session->set_flashdata('delete_failed','Failed, Please Try Again!');
			redirect('/distribution');
		}
	}

	
}
