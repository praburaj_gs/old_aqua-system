<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pondcleaning extends MY_Controller {
		 
/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
  public function __construct(){
  parent::__construct();
  $this->load->helper('date');
  $this->table="pond_cleaning";
 }

	public function index()
	{
		if($this->fb_rest->isloggedin()){
			$table_name='ponds';
			$cmb_list = fb_combo_arr($table_name);
			$data = array();
			$page_no = $this->uri->segment('2');
			$per_page = $this->input->get_post("no_items", true);
		//	$per_page =2;
			$search = $this->input->get_post("search", true);
			$sort_fld = $this->input->get_post("sort_fld", true);
			$sort_dir = $this->input->get_post("sort_dir", true);
			$page_burl = site_url("/pondcleaning");
			$table_name = $this->table;
			$params =  array("page_no" => $page_no, "per_page" => $per_page, "uri_segment" => "2",
			"search" => $search, "sort_fld" => $sort_fld, "sort_dir" => $sort_dir, "page_burl" => $page_burl, "table_name" => $table_name);
			//$params = array();
			// or  $params = compact("page_no", "per_page", "search", "sort_fld", "sort_dir", "page_burl", "table_name");
			$data["sort_fld"] = $sort_fld;
			$data["sort_dir"] = $sort_dir;
			$data["search"] = $search;
			$data["per_page"] = $per_page;
			
			$sort_columns = array("pondname", "cleanedby", "cleaned_date");
			
			$hstr = array("pondname" => fb_text("pond_name"), "cleanedby" =>fb_text("cleaned_by"),
			"cleaned_date" => fb_text("cleaned_date"),
			"action" => fb_text("action"),
			);
			
			$theader = "";
			
			foreach($hstr as $hk => $hv)
			{
				if(in_array($hk, $sort_columns)){
					$cdir = ($hk == $sort_fld) ? (($sort_dir=="asc") ? "desc" : "asc" ) : "asc";
					$pstr = (!empty($per_page)) ? $per_page : "10";
					$srt_params = array("sort_fld" => $hk, "sort_dir" => $cdir, "no_items" => $pstr, "search" => $search);
					$srt_str = http_build_query($srt_params);
					$srt_url = site_url("/pondcleaning?$srt_str");
					$cdir_icon = "";
					if(!empty($sort_fld)){
						$cdir_icon = ($hk == $sort_fld) ? 
						(($sort_dir=="asc") ? "&nbsp;<i class=\"fa fa-sort-asc\"></i>" : "&nbsp;<i class=\"fa fa-sort-desc\"></i>" ) : "";
					}
					$thstr = $hv.$cdir_icon;
					$thtml = "<th><a href='$srt_url'>$thstr</a></th>";
					$theader .= $thtml."\n";
				}else{
					$theader .= "<th>$hv</th>\n";
				}
			}
			
			$data["theader"] = $theader;
		
			$msg  = $this->fb_rest->list_record($params);
			
  		    $this->load->view('include/header');
			$this->load->view('include/left_menu');
//			fb_pr($msg);

			if($msg["status"] == "success")
			{
				$data['cmb_list'] = $cmb_list;
				$data["page_links"] = $msg["page_links"];
				$data["result_set"] = $msg["result_set"];
				$this->load->view("layout/pondcleaning_content", $data);
			}else{
				//$this->load->view("error", $data);
			}

			$this->load->view('include/footer');
		}else{
			redirect('/login');
		}
	}

	function create(){
		$table_name=$this->table;
		$form_data = $this->input->post();
		$form_data['cleaned_date'] = fb_convert_time($form_data['cleaned_date']);
		$form_data['createdtime']=now();
		$form_data['updatedtime']=now();		
		// Pond id and name update
		$pid = $form_data["pondname"]; // Actually get pond id		
		$crst = $this->fb_rest->get_record("ponds", $pid);
		$rst = $crst["result_set"];
		$pname = $rst["pondname"];
		$form_data["pondname"] = $pname;
		$form_data["pond_id"] = $pid;

		$result = $this->fb_rest->create_record($table_name,$form_data);
//		print_r($result);
		if($result['status']=="success"){
		$this->session->set_flashdata('success',fb_text("success"));
		redirect('/pondcleaning');
		}else{
		$this->session->set_flashdata('failed',fb_text("failed"));
		redirect('/pondcleaning');
		}
	}
	
	function delete(){
		$table_name=$this->table;
		$rkey = $this->input->post("rid");	
		$result= $this->fb_rest->delete_record($table_name, $rkey);
		
		if($result['status']=="success"){
			$this->session->set_flashdata('delete_success',fb_text("delete_success"));
			redirect('/pondcleaning');
		}else{
			$this->session->set_flashdata('delete_failed',fb_text("delete_failed"));
			redirect('/pondcleaning');
		}
	}
	
	function editPondcleaning(){
		$data = array();
		$table_name=$this->table;
		$rkey = $this->input->post("rid");
		$record= $this->fb_rest->get_record($table_name, $rkey);
		if($record["status"] == "success")
		 {	
		 	$data['record'] = $record["result_set"];
			$data['rkey'] = $rkey;
			$this->load->view("layout/edit/pondcleaning_edit_content", $data);
		 }
		
	}
	function update(){
		$table_name=$this->table;
		$form_data = $this->input->post();
		$form_data['updatedtime']=now();	
		$cleaned_date = $this->input->post("cleaned_date", true);
		$form_data['cleaned_date'] = fb_convert_time($cleaned_date);
		$rkey = $this->input->post("rkey");		

		// Pond id and name update
		$pid = $form_data["pondname"]; // Actually get pond id		
		$crst = $this->fb_rest->get_record("ponds", $pid);
		$rst = $crst["result_set"];
		$pname = $rst["pondname"];
		$form_data["pondname"] = $pname;
		$form_data["pond_id"] = $pid;

		$result = $this->fb_rest->update_record($table_name,$form_data,$rkey);
//		print_r($result);
		if($result['status']=="success"){
		$this->session->set_flashdata('update_success',fb_text("update_success"));
		redirect('/pondcleaning');
		}else{
		$this->session->set_flashdata('update_failed',fb_text("update_failed"));
		redirect('/pondcleaning');
		}
	}


}
