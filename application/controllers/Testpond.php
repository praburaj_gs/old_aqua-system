<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testpond extends MY_Controller {

	protected $table;

    public function __construct()
	{
        parent::__construct();
		if(!$this->fb_rest->isloggedin()){
			redirect("/");
		}
		$this->table = "ponds1";
	}
	
	public function index(){
		$data = array();
		$page_no = $this->uri->segment('2');
		$per_page = $this->input->get_post("no_items", true);
		$search = $this->input->get_post("search", true);
		$sort_fld = $this->input->get_post("sort_fld", true);
		$sort_dir = $this->input->get_post("sort_dir", true);
		$page_burl = site_url("/testpond");
		$table_name = "ponds1";
		$params =  array("page_no" => $page_no, "per_page" => $per_page, "uri_segment" => "2",
		"search" => $search, "sort_fld" => $sort_fld, "sort_dir" => $sort_dir, "page_burl" => $page_burl, "table_name" => $table_name);
		//$params = array();
		// or  $params = compact("page_no", "per_page", "search", "sort_fld", "sort_dir", "page_burl", "table_name");
		
		$data["sort_fld"] = $sort_fld;
		$data["sort_dir"] = $sort_dir;
		$data["search"] = $search;
		$data["per_page"] = $per_page;
		
		$sort_columns = array("pondname", "height", "width", "depth","built_date");
		
		$hstr = array("pondname" => fb_text("pond_name"), "height" => fb_text("height"), "width" => fb_text("width"),
		"depth" => fb_text("depth"),
		"built_date" => fb_text("date"),
		"action" => fb_text("action"),
		);
		
		$theader = "";
		
		foreach($hstr as $hk => $hv)
		{
			if(in_array($hk, $sort_columns)){
				$cdir = ($hk == $sort_fld) ? (($sort_dir=="asc") ? "desc" : "asc" ) : "asc";
				$pstr = (!empty($per_page)) ? $per_page : "10";
				$srt_params = array("sort_fld" => $hk, "sort_dir" => $cdir, "no_items" => $pstr, "search" => $search);
				$srt_str = http_build_query($srt_params);
				$srt_url = site_url("/testpond?$srt_str");
				$cdir_icon = "";
				if(!empty($sort_fld)){
					$cdir_icon = ($hk == $sort_fld) ? 
					(($sort_dir=="asc") ? "&nbsp;<i class=\"fa fa-sort-asc\"></i>" : "&nbsp;<i class=\"fa fa-sort-desc\"></i>" ) : "";
				}
				$thstr = $hv.$cdir_icon;
				$thtml = "<th><a href='$srt_url'>$thstr</a></th>";
				$theader .= $thtml."\n";
			}else{
				$theader .= "<th>$hv</th>\n";
			}
		}
		
		$data["theader"] = $theader;
		  
		$msg  = $this->fb_rest->list_record($params);
		
		if($msg["status"] == "success")
		{
			$data["page_links"] = $msg["page_links"];
			$data["result_set"] = $msg["result_set"];
			$this->load->view("testpond", $data);
		}else{
			//$this->load->view("error", $data);
		}
		
	}
	
	function create(){
		$table_name = $this->table;
		$form_data = $this->input->post();
		$built_date = $this->input->post("built_date", true);
		$form_data['built_date'] = fb_convert_time($built_date);
		$form_data['createdtime'] = now();
		$form_data['updatedtime'] = now();		
		$result = $this->fb_rest->create_record($table_name,$form_data);

		if($result['status']=="success"){
			$this->session->set_flashdata('success','Record Created');
			redirect('/testpond');
		}else{
			$this->session->set_flashdata('failed','Insert failed, Try Again!');
			redirect('/testpond');
		}
	}
	
	function update(){
		$table_name = $this->table;
		$form_data = $this->input->post();
		$form_data['updatedtime'] = now();
		$rkey = $this->input->post_get("rkey", true);
		$result = $this->fb_rest->update_record($table_name, $form_data, $rkey);

		if($result['status']=="success"){
			$this->session->set_flashdata('success','Record Updated successfully');
			redirect('/testpond');
		}else{
			$this->session->set_flashdata('failed','Insert failed, Try Again!');
			redirect('/testpond');
		}
	}
	
	public function delete_pond(){
		$rkey = $this->uri->segment('3','0');
		$table_name = $this->table;
		$this->fb_rest->delete_record($table_name, $rkey);
		$this->session->set_flashdata('success','Record deleted successfully');
		redirect('/testpond');
	}
	
	
}