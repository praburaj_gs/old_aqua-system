<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Currentstock extends MY_Controller {
	
	public function __construct(){
		parent::__construct();
		
		if(!$this->fb_rest->isloggedin()){
			redirect('/login');
		}
		
		$this->table = "current_stock";
	}
	
	public function index()
	{
		$data = array();
		$page_no = $this->uri->segment('2');
		$per_page = $this->input->get_post("per_page", true);
		$search = $this->input->get_post("search", true);
		$sort_fld = $this->input->get_post("sort_fld", true);
		$sort_dir = $this->input->get_post("sort_dir", true);
		$page_burl = site_url("/stock");
		$table_name = $this->table;
		$params =  array("page_no" => $page_no, "per_page" => $per_page, "uri_segment" => "2",
		"search" => $search, "sort_fld" => $sort_fld, "sort_dir" => $sort_dir, "page_burl" => $page_burl, "table_name" => $table_name);
		
		
		  
		$msg  = $this->fb_rest->list_record($params);
		
		
		$this->load->view('include/header');
		$this->load->view('include/left_menu');
		
	
		if($msg["status"] == "success")
		{
			$data["page_links"] = $msg["page_links"];
			$data["result_set"] = $msg["result_set"];
			$this->load->view("layout/curstock_content", $data);
		}else{
			//$this->load->view("error", $data);
		}
		
		$this->load->view('include/footer');
	}
	
}