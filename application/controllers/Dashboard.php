<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct() {
		parent::__construct();
	}

	public function index()
	{
		$data = array();
		$cstockTable = "current_stock";
		$stock = $this->fb_rest->search_list($cstockTable,'');	
		$data['stocks'] = $stock["result_set"];				
		if($this->fb_rest->isloggedin()){
  		    $this->load->view('include/header');
			$this->load->view('include/left_menu');
			$this->load->view('layout/dashboard_content',$data);
			$this->load->view('include/footer');
		}else{
			redirect('/login');
		}
	}
	
	public function getCurrentStock(){
		$table_name = "current_stock";
		$stocks = $this->fb_rest->search_list($table_name,'');		
		echo json_encode($stocks);
	}
	
	
}
