<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sampling extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct(){
	  parent::__construct();
	  $this->load->helper('date');
	  $table_name = "sampling";
	}
	public function index()
	{
		if($this->fb_rest->isloggedin()){
			$this->load->view('include/header');
			$this->load->view('include/left_menu');
			
			$this->load->view('include/footer');
		}else{
			redirect('/login');
		}
	}
	public function add_record(){
		$table_name = "sampling";
		$form_data = $this->input->post();
		$form_data['sampling_state']="true";
		$sampling_date = $this->input->post("sampling_date", true);
		$form_data['sampling_date'] = fb_convert_time($sampling_date);
		$form_data['createdtime']=now();
		$form_data['updatedtime']=now();
		$result = $this->fb_rest->create_record($table_name, $form_data);
		fb_pr($result);
		if($result['status']=="success"){
			$this->session->set_flashdata('success',fb_text("success"));
			redirect('/currentstock');
		}else{
			$this->session->set_flashdata('failed',fb_text("failed"));
			redirect('/currentstock');
		} 
	}
	function search_list()
	{
		$data = array();
		$table_name = "sampling";
		$search = $this->input->get_post("pondname", true);
		$record = $this->fb_rest->search_list($table_name,$search, 1);
		echo json_encode($record);
		//fb_pr($record);
	}
	public function list_record(){
		$table_name = "sampling";
		$this->fb_rest->test_list_record($table_name);
	}
	
}
