# Swagger\Client\DeleteStoredDomainPropertyApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteDomainProperty**](DeleteStoredDomainPropertyApi.md#deleteDomainProperty) | **DELETE** /domain/property/delete/{atoken}/{name} | Delete Stored Domain Property


# **deleteDomainProperty**
> \Swagger\Client\Model\Success deleteDomainProperty($atoken, $name)

Delete Stored Domain Property

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\DeleteStoredDomainPropertyApi();
$atoken = "atoken_example"; // string | Auth token of the logged in domain
$name = "name_example"; // string | 

try {
    $result = $api_instance->deleteDomainProperty($atoken, $name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeleteStoredDomainPropertyApi->deleteDomainProperty: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in domain |
 **name** | **string**|  |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

