# Swagger\Client\DeleteAllStoredDomainPropertiesApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteAllDomainProperties**](DeleteAllStoredDomainPropertiesApi.md#deleteAllDomainProperties) | **DELETE** /domain/property/deleteall/{atoken} | Delete All Stored Domain Properties


# **deleteAllDomainProperties**
> \Swagger\Client\Model\Success deleteAllDomainProperties($atoken)

Delete All Stored Domain Properties

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\DeleteAllStoredDomainPropertiesApi();
$atoken = "atoken_example"; // string | Auth token of the logged in domain

try {
    $result = $api_instance->deleteAllDomainProperties($atoken);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeleteAllStoredDomainPropertiesApi->deleteAllDomainProperties: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in domain |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

