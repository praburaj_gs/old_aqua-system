# Swagger\Client\DeleteRecordApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteRecord**](DeleteRecordApi.md#deleteRecord) | **DELETE** /reocrd/delete/{atoken}/{rid}/{rkey} | Delete Record


# **deleteRecord**
> \Swagger\Client\Model\Success deleteRecord($atoken, $rid, $rkey)

Delete Record

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\DeleteRecordApi();
$atoken = "atoken_example"; // string | Auth token of the logged in user
$rid = 56; // int | Well defined unique record ID
$rkey = "rkey_example"; // string | Record's unique identifier

try {
    $result = $api_instance->deleteRecord($atoken, $rid, $rkey);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeleteRecordApi->deleteRecord: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **rid** | **int**| Well defined unique record ID |
 **rkey** | **string**| Record&#39;s unique identifier |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

