# Swagger\Client\InsertUpdateRecordApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**upsertRecord**](InsertUpdateRecordApi.md#upsertRecord) | **POST** /reocrd/insert/dynamic/{atoken}/{rid} | Insert/Update Record


# **upsertRecord**
> \Swagger\Client\Model\InsertResult upsertRecord($atoken, $rid, $data)

Insert/Update Record

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\InsertUpdateRecordApi();
$atoken = "atoken_example"; // string | Auth token of the logged in user
$rid = 56; // int | Well defined unique record ID
$data = "data_example"; // string | Stringified record JSON object

try {
    $result = $api_instance->upsertRecord($atoken, $rid, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InsertUpdateRecordApi->upsertRecord: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **rid** | **int**| Well defined unique record ID |
 **data** | **string**| Stringified record JSON object |

### Return type

[**\Swagger\Client\Model\InsertResult**](../Model/InsertResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

