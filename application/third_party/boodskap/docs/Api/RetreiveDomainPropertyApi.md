# Swagger\Client\RetreiveDomainPropertyApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDomainProperty**](RetreiveDomainPropertyApi.md#getDomainProperty) | **GET** /domain/property/get/{atoken}/{name} | Retreive Domain Property


# **getDomainProperty**
> \Swagger\Client\Model\DomainProperty getDomainProperty($atoken, $name)

Retreive Domain Property

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\RetreiveDomainPropertyApi();
$atoken = "atoken_example"; // string | Auth token of the logged in domain
$name = "name_example"; // string | 

try {
    $result = $api_instance->getDomainProperty($atoken, $name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RetreiveDomainPropertyApi->getDomainProperty: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in domain |
 **name** | **string**|  |

### Return type

[**\Swagger\Client\Model\DomainProperty**](../Model/DomainProperty.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

