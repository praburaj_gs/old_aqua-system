# TensorFlowModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**model** | **string** |  | 
**description** | **string** |  | [optional] 
**font_face** | **int** |  | [optional] 
**font_scale** | **double** |  | [optional] 
**min_score** | **double** |  | [optional] 
**overlay_text** | **bool** |  | [optional] 
**overlay_type** | **string** |  | [optional] 
**graph_size** | **int** |  | [optional] 
**label_size** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


