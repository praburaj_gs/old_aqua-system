# UserDomain

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user** | [**\Swagger\Client\Model\User**](User.md) |  | 
**domain** | [**\Swagger\Client\Model\Domain**](Domain.md) |  | 
**token** | **string** |  | 
**domain_key** | **string** |  | 
**api_key** | **string** |  | 
**label** | **string** |  | [optional] 
**linked_domains** | [**\Swagger\Client\Model\LinkedDomain[]**](LinkedDomain.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


