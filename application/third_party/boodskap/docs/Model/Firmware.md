# Firmware

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**device_model** | **string** |  | 
**version** | **string** |  | 
**description** | **string** |  | [optional] 
**file_name** | **string** |  | [optional] 
**content_type** | **string** |  | [optional] 
**create_at** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


