# Device

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | 
**model_id** | **string** |  | 
**version** | **string** |  | 
**name** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**registered_stamp** | **int** |  | [optional] 
**password** | **string** |  | [optional] 
**asset_id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


