# FRModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | 
**algorithm** | **string** |  | 
**image_width** | **int** |  | 
**image_height** | **int** |  | 
**train_every** | **int** |  | 
**created_at** | **int** |  | [optional] 
**trained_at** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


