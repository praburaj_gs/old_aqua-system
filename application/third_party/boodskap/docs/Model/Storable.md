# Storable

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data_type** | **string** |  | [optional] 
**format** | **string** | Required if **dataType** is **blob** | [optional] 
**label** | **string** |  | [optional] 
**description** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


