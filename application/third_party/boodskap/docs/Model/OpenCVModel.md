# OpenCVModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**model** | **string** |  | 
**description** | **string** |  | [optional] 
**state** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


