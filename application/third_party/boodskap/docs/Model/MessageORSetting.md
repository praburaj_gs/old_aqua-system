# MessageORSetting

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message_id** | **int** |  | 
**message_field** | **string** |  | 
**or_type** | **string** |  | 
**or_model** | **string** |  | 
**named_rule** | **string** |  | [optional] 
**description** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


