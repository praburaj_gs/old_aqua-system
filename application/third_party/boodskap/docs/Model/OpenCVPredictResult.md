# OpenCVPredictResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**labels** | [**\Swagger\Client\Model\OpenCVORLabel[]**](OpenCVORLabel.md) |  | 
**image** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


