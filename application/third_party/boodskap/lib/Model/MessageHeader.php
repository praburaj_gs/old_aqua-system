<?php
/**
 * MessageHeader
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Domain API
 *
 * Boodskap IoT Platform (Domain API)
 *
 * OpenAPI spec version: 1.0.6
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;

/**
 * MessageHeader Class Doc Comment
 *
 * @category    Class
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class MessageHeader implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'MessageHeader';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'key' => 'string',
        'api' => 'string',
        'did' => 'string',
        'dmdl' => 'string',
        'fwver' => 'string',
        'mid' => 'int'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerFormats = [
        'key' => null,
        'api' => null,
        'did' => null,
        'dmdl' => null,
        'fwver' => null,
        'mid' => 'int32'
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'key' => 'key',
        'api' => 'api',
        'did' => 'did',
        'dmdl' => 'dmdl',
        'fwver' => 'fwver',
        'mid' => 'mid'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'key' => 'setKey',
        'api' => 'setApi',
        'did' => 'setDid',
        'dmdl' => 'setDmdl',
        'fwver' => 'setFwver',
        'mid' => 'setMid'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'key' => 'getKey',
        'api' => 'getApi',
        'did' => 'getDid',
        'dmdl' => 'getDmdl',
        'fwver' => 'getFwver',
        'mid' => 'getMid'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['key'] = isset($data['key']) ? $data['key'] : null;
        $this->container['api'] = isset($data['api']) ? $data['api'] : null;
        $this->container['did'] = isset($data['did']) ? $data['did'] : null;
        $this->container['dmdl'] = isset($data['dmdl']) ? $data['dmdl'] : null;
        $this->container['fwver'] = isset($data['fwver']) ? $data['fwver'] : null;
        $this->container['mid'] = isset($data['mid']) ? $data['mid'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        if ($this->container['key'] === null) {
            $invalid_properties[] = "'key' can't be null";
        }
        if ($this->container['api'] === null) {
            $invalid_properties[] = "'api' can't be null";
        }
        if ($this->container['did'] === null) {
            $invalid_properties[] = "'did' can't be null";
        }
        if ($this->container['dmdl'] === null) {
            $invalid_properties[] = "'dmdl' can't be null";
        }
        if ($this->container['fwver'] === null) {
            $invalid_properties[] = "'fwver' can't be null";
        }
        if ($this->container['mid'] === null) {
            $invalid_properties[] = "'mid' can't be null";
        }
        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        if ($this->container['key'] === null) {
            return false;
        }
        if ($this->container['api'] === null) {
            return false;
        }
        if ($this->container['did'] === null) {
            return false;
        }
        if ($this->container['dmdl'] === null) {
            return false;
        }
        if ($this->container['fwver'] === null) {
            return false;
        }
        if ($this->container['mid'] === null) {
            return false;
        }
        return true;
    }


    /**
     * Gets key
     * @return string
     */
    public function getKey()
    {
        return $this->container['key'];
    }

    /**
     * Sets key
     * @param string $key domain key
     * @return $this
     */
    public function setKey($key)
    {
        $this->container['key'] = $key;

        return $this;
    }

    /**
     * Gets api
     * @return string
     */
    public function getApi()
    {
        return $this->container['api'];
    }

    /**
     * Sets api
     * @param string $api domain api-key
     * @return $this
     */
    public function setApi($api)
    {
        $this->container['api'] = $api;

        return $this;
    }

    /**
     * Gets did
     * @return string
     */
    public function getDid()
    {
        return $this->container['did'];
    }

    /**
     * Sets did
     * @param string $did device id
     * @return $this
     */
    public function setDid($did)
    {
        $this->container['did'] = $did;

        return $this;
    }

    /**
     * Gets dmdl
     * @return string
     */
    public function getDmdl()
    {
        return $this->container['dmdl'];
    }

    /**
     * Sets dmdl
     * @param string $dmdl device model
     * @return $this
     */
    public function setDmdl($dmdl)
    {
        $this->container['dmdl'] = $dmdl;

        return $this;
    }

    /**
     * Gets fwver
     * @return string
     */
    public function getFwver()
    {
        return $this->container['fwver'];
    }

    /**
     * Sets fwver
     * @param string $fwver device firmware version
     * @return $this
     */
    public function setFwver($fwver)
    {
        $this->container['fwver'] = $fwver;

        return $this;
    }

    /**
     * Gets mid
     * @return int
     */
    public function getMid()
    {
        return $this->container['mid'];
    }

    /**
     * Sets mid
     * @param int $mid message identifier
     * @return $this
     */
    public function setMid($mid)
    {
        $this->container['mid'] = $mid;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this));
    }
}


