<?php
/**
 * ClusterNode
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Domain API
 *
 * Boodskap IoT Platform (Domain API)
 *
 * OpenAPI spec version: 1.0.6
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;

/**
 * ClusterNode Class Doc Comment
 *
 * @category    Class
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ClusterNode implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'ClusterNode';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'node_id' => 'string',
        'node_uid' => 'string',
        'consistent_id' => 'string',
        'addresses' => 'string[]',
        'host_names' => 'string[]',
        'props' => '\Swagger\Client\Model\NodeProperty[]'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerFormats = [
        'node_id' => null,
        'node_uid' => null,
        'consistent_id' => null,
        'addresses' => null,
        'host_names' => null,
        'props' => null
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'node_id' => 'nodeId',
        'node_uid' => 'nodeUid',
        'consistent_id' => 'consistentId',
        'addresses' => 'addresses',
        'host_names' => 'hostNames',
        'props' => 'props'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'node_id' => 'setNodeId',
        'node_uid' => 'setNodeUid',
        'consistent_id' => 'setConsistentId',
        'addresses' => 'setAddresses',
        'host_names' => 'setHostNames',
        'props' => 'setProps'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'node_id' => 'getNodeId',
        'node_uid' => 'getNodeUid',
        'consistent_id' => 'getConsistentId',
        'addresses' => 'getAddresses',
        'host_names' => 'getHostNames',
        'props' => 'getProps'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['node_id'] = isset($data['node_id']) ? $data['node_id'] : null;
        $this->container['node_uid'] = isset($data['node_uid']) ? $data['node_uid'] : null;
        $this->container['consistent_id'] = isset($data['consistent_id']) ? $data['consistent_id'] : null;
        $this->container['addresses'] = isset($data['addresses']) ? $data['addresses'] : null;
        $this->container['host_names'] = isset($data['host_names']) ? $data['host_names'] : null;
        $this->container['props'] = isset($data['props']) ? $data['props'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        return true;
    }


    /**
     * Gets node_id
     * @return string
     */
    public function getNodeId()
    {
        return $this->container['node_id'];
    }

    /**
     * Sets node_id
     * @param string $node_id
     * @return $this
     */
    public function setNodeId($node_id)
    {
        $this->container['node_id'] = $node_id;

        return $this;
    }

    /**
     * Gets node_uid
     * @return string
     */
    public function getNodeUid()
    {
        return $this->container['node_uid'];
    }

    /**
     * Sets node_uid
     * @param string $node_uid
     * @return $this
     */
    public function setNodeUid($node_uid)
    {
        $this->container['node_uid'] = $node_uid;

        return $this;
    }

    /**
     * Gets consistent_id
     * @return string
     */
    public function getConsistentId()
    {
        return $this->container['consistent_id'];
    }

    /**
     * Sets consistent_id
     * @param string $consistent_id
     * @return $this
     */
    public function setConsistentId($consistent_id)
    {
        $this->container['consistent_id'] = $consistent_id;

        return $this;
    }

    /**
     * Gets addresses
     * @return string[]
     */
    public function getAddresses()
    {
        return $this->container['addresses'];
    }

    /**
     * Sets addresses
     * @param string[] $addresses
     * @return $this
     */
    public function setAddresses($addresses)
    {
        $this->container['addresses'] = $addresses;

        return $this;
    }

    /**
     * Gets host_names
     * @return string[]
     */
    public function getHostNames()
    {
        return $this->container['host_names'];
    }

    /**
     * Sets host_names
     * @param string[] $host_names
     * @return $this
     */
    public function setHostNames($host_names)
    {
        $this->container['host_names'] = $host_names;

        return $this;
    }

    /**
     * Gets props
     * @return \Swagger\Client\Model\NodeProperty[]
     */
    public function getProps()
    {
        return $this->container['props'];
    }

    /**
     * Sets props
     * @param \Swagger\Client\Model\NodeProperty[] $props
     * @return $this
     */
    public function setProps($props)
    {
        $this->container['props'] = $props;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this));
    }
}


