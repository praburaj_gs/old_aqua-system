<?php
/**
 * LocationTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Domain API
 *
 * Boodskap IoT Platform (Domain API)
 *
 * OpenAPI spec version: 1.0.6
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * LocationTest Class Doc Comment
 *
 * @category    Class */
// * @description Location
/**
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class LocationTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Location"
     */
    public function testLocation()
    {
    }

    /**
     * Test attribute "asset_id"
     */
    public function testPropertyAssetId()
    {
    }

    /**
     * Test attribute "at"
     */
    public function testPropertyAt()
    {
    }

    /**
     * Test attribute "lat"
     */
    public function testPropertyLat()
    {
    }

    /**
     * Test attribute "lon"
     */
    public function testPropertyLon()
    {
    }

    /**
     * Test attribute "device_id"
     */
    public function testPropertyDeviceId()
    {
    }
}
