<div id="right-panel" class="right-panel">
  <?php $this->load->view('./include/top_menu'); ?>
  <div class="breadcrumbs">
    <div class="col-sm-4">
      <div class="page-header float-left">
        <div class="page-title">
          <h1><?php echo fb_text("species"); ?></h1>
        </div>
      </div>
    </div>
    <div class="col-sm-8">
      <div class="page-header float-right">
        <div class="page-title">
          <ol class="breadcrumb text-right">
            <li><a href="#"><?php echo fb_text("dashboard"); ?></a></li>
            <li class="active"><?php echo fb_text("species"); ?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="content mt-3">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header"> <strong class="card-title"><?php echo fb_text("species"); ?></strong> </div>
            <div class="card-body">
             <?php if($this->session->flashdata('delete_success')) {
			  ?>
			  <div class="sufee-alert alert with-close alert-success alert-dismissible fade show"> <?php echo $this->session->flashdata('delete_success');  ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <?php } ?>
			  <?php if($this->session->flashdata('delete_failed')) {
			  ?>
			  <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert"> <?php echo $this->session->flashdata('delete_failed');  ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <?php } ?>               
              <div id="bootstrap-data-table_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
			  <form name="search" id="srchfrm" action="<?php echo site_url("/species/"); ?>" method="GET">
				<div class="row">
                  <div class="col-sm-12 col-md-6">
                    <div class="dataTables_length" id="bootstrap-data-table_length">
                      <label> Show
                        <select name="no_items" aria-controls="bootstrap-data-table" class="form-control form-control-sm">
                          <option value="10">10</option>
                          <option value="20">20</option>
                          <option value="50">50</option>
                          <option value="-1">All</option>
                        </select>
                        entries </label>
                    </div>
                  </div>
                  <div class="col-sm-12 col-md-6">
                    <div id="bootstrap-data-table_filter" class="dataTables_filter float-right">
                      <label>Search:
                        <input type="search" name="search" class="form-control form-control-sm" placeholder="" value="<?php echo $search; ?>" />
                      </label>
                    </div>
                    <input type="hidden" name="sort_fld" value="<?php echo $sort_fld; ?>"/>
                    <input type="hidden" name="sort_dir" value="<?php echo $sort_dir; ?>"/>
                  </div>
                </div>
				</form>
                <table id="bootstrap-data-table" class="table table-striped table-bordered" role="grid">
                  <thead>
						   <tr role="row">
							  <?php echo $theader; ?>
						   </tr>
						</thead>
                  <tbody>
                    <?php foreach($result_set as $row): 
								  $source = $row["_source"];
								  $rkey = $row["_id"];
							   ?>
                    <tr role="row">
                      <td class=""><?php echo $source["species_type"]; ?></td>
                      <td><?php echo $source["feedname"]; ?></td>
                      <td><?php echo $source["max_weight"]; ?></td>
                     <td><a href="#" data-id="" class="view-modal" data-toggle="modal" data-target="#view-modal"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="javascript:void(0);" data-id="<?=$rkey?>" class="edit-species" ><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#" data-id="<?=$rkey?>" class="delete-modal" data-toggle="modal" data-target="#delete-modal"><i class="fa fa-trash"></i></a></td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
                <div class="row">
                  <div class="col-sm-12 col-md-5"> 
                    <!--<div class="dataTables_info" id="bootstrap-data-table_info" role="status" aria-live="polite">Showing 11 to 20 of 25 entries</div>--> 
                  </div>
                  <div class="col-sm-12 col-md-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="bootstrap-data-table_paginate"> <?php echo $page_links; ?> </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="card" id="speciesform-card">
            <div class="card-header"> <strong class="card-title"><?php echo fb_text("add_species"); ?></strong> </div>
            <div class="card-body">
              <form name="species" id="species-form" method="post" action="<?php echo base_url('species/add_record');?>">
                <?php if($this->session->flashdata('success')) {
				?>
				<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">                       
				<?php echo $this->session->flashdata('success');  ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
				</div> 
				<?php } ?>                 
				
				<?php if($this->session->flashdata('failed')) {
				?>
				<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert">
				<?php echo $this->session->flashdata('failed');  ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
				</div> 
				<?php } ?>   
				 <?php if($this->session->flashdata('update_success')) {
				?>
					<div class="sufee-alert alert with-close alert-success alert-dismissible fade show"> <?php echo $this->session->flashdata('update_success');  ?>
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div>
					<?php } ?>
					<?php if($this->session->flashdata('update_failed')) {
				?>
					<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert"> <?php echo $this->session->flashdata('update_failed');  ?>
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div>
                <?php } ?>    
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="speciestype"><?php echo fb_text("species_type"); ?><span class="error">*</span></label>
                    <input type="text" name="speciestype" class="form-control" placeholder="<?php echo fb_text("species_name"); ?>"/>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="feedname"><?php echo fb_text("feed_name"); ?><span class="error">*</span></label>
                    <?php
						echo "<select id=\"feedname\" name=\"feedname\" class=\"form-control\">";
						echo '<option value="" selected>'.fb_text("choose").'</option>';
						foreach($cmb_list as $ck => $cv){
						echo "<option value='$ck'>$cv</option>";
						}
						echo "</select>";
					?>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="maxweight"><?php echo fb_text("market_size"); ?><span class="error">*</span></label>
                    <input type="text" name="maxweight" class="form-control" placeholder="<?php echo fb_text("size"); ?>"/>
                  </div>
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="reset" class="btn btn-secondary">Cancel</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .animated --> 
  </div>
</div>
<!-- /#right-panel --> 

<!-- aaRight Panel --> 
<!-- Delete Modal --> 
<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticModalLabel">Delete</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p>
               Are you sure to delete this?
            </p>
        </div>
        <div class="modal-footer">
        	<form method="post"  action="<?php echo base_url('species/delete');?>">
            <input type="hidden" name="rid" id="delete_rid"/>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Confirm</button>
            </form>
        </div>
    </div>
</div>
</div>

<!-- View Modal --> 
<div class="modal fade" id="view-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="speciesnamemodel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p><label><?php echo fb_text("feed_name"); ?>:</label> &nbsp;&nbsp; <span id="speciesfeedmodel"></span></p>
            <p><label><?php echo fb_text("market_size"); ?>:</label>&nbsp;&nbsp; <span id="speciesmaxweightmodel"></span></p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>