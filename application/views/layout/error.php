<div id="right-panel" class="right-panel">
  <?php $this->load->view('./include/top_menu'); ?>
  <div class="content mt-3">
    <div class="row">
      <div class="col-md-6 offset-md-3 mr-auto ml-auto">
        <div class="card">
          <div class="card-header bg-dark"> <strong class="card-title text-light">Api Error</strong> </div>
          <div class="card-body text-white bg-danger">
            <p class="card-text text-light">Error connecting to the API,. Please try again later</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /#right-panel --> 

<!-- Right Panel -->