<div id="right-panel" class="right-panel">
  <?php $this->load->view('./include/top_menu'); ?>
  <div class="breadcrumbs">
    <div class="col-sm-4">
      <div class="page-header float-left">
        <div class="page-title">
          <h1><?php echo fb_text("stock"); ?></h1>
        </div>
      </div>
    </div>
    <div class="col-sm-8">
      <div class="page-header float-right">
        <div class="page-title">
          <ol class="breadcrumb text-right">
            <li><a href="#"><?php echo fb_text("dashboard"); ?></a></li>
            <li class="active"><?php echo fb_text("stock"); ?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="content mt-3">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header"> <strong class="card-title"><?php echo fb_text("stock"); ?></strong> </div>
            <div class="card-body">
             <?php if($this->session->flashdata('delete_success')) {
			  ?>
			  <div class="sufee-alert alert with-close alert-success alert-dismissible fade show"> <?php echo $this->session->flashdata('delete_success');  ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <?php } ?>
			  <?php if($this->session->flashdata('delete_failed')) {
			  ?>
			  <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert"> <?php echo $this->session->flashdata('delete_failed');  ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <?php } ?>       
              <div id="bootstrap-data-table_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                <div class="row">
                  <div class="col-sm-12 col-md-6">
                    <div class="dataTables_length" id="bootstrap-data-table_length">
                      <label> Show
                        <select name="per_page" aria-controls="bootstrap-data-table" class="form-control form-control-sm">
                          <option value="10">10</option>
                          <option value="20">20</option>
                          <option value="50">50</option>
                          <option value="-1">All</option>
                        </select>
                        entries </label>
                    </div>
                  </div>
                  <div class="col-sm-12 col-md-6">
                    <div id="bootstrap-data-table_filter" class="dataTables_filter float-right">
                      <label>Search:
                        <input type="search" name="search" class="form-control form-control-sm" placeholder="" >
                      </label>
                    </div>
                    <input type="hidden" name="sort_fld" value="">
                    <input type="hidden" name="sort_dir" value="">
                  </div>
                </div>
                <div class="row"> 
                  <div class="col-sm-12">           
					 <table class="table table-striped table-bordered no-footer" role="grid">
						<thead>
						   <tr role="row">
							  <th><?php echo fb_text("pond_name"); ?></th>
                              <th><?php echo fb_text("count"); ?></th>
							  <th><?php echo fb_text("species_type"); ?></th>
							  <th><?php echo fb_text("weight"); ?></th>
							  <th><?php echo fb_text("fertilizer"); ?></th>
                              <th><?php echo fb_text("water_type"); ?></th>
                              <th><?php echo fb_text("date"); ?></th>
							  <th><?php echo fb_text("action"); ?></th>
						   </tr>
						</thead>
						<tbody>
						   
						   <?php foreach($result_set as $row): 
							  $source = $row["_source"];
							  $rkey = $row["_id"];
						   ?>
						     <tr role="row">
							  <td class=""><?php echo $source["pondname"]; ?></td>
                              <td><?php echo $source["count"]; ?></td>
							  <td><?php echo $source["species_type"]; ?></td>
							  <td><?php echo $source["weight"]; ?></td>
                              <td><?php echo $source["fertilizer"]; ?></td>
                              <td><?php echo $source["water_type"]; ?></td>
							  <td class=""><?php echo $source["_month"]."/".$source["_day"]."/".$source["_year"]; ?></td>
							  <td>
							  <a href="#" data-id="" class="view-modal" data-toggle="modal" data-target="#view-modal"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; 
							  <a href="#" data-id="<?=$rkey?>" class="delete-modal" data-toggle="modal" data-target="#delete-modal"><i class="fa fa-trash"></i></a></td>
						    </tr>
						   <?php endforeach; ?>
						</tbody>
					 </table>
				  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 col-md-5"> 
                    <!--<div class="dataTables_info" id="bootstrap-data-table_info" role="status" aria-live="polite">Showing 11 to 20 of 25 entries</div>--> 
                  </div>
                  <div class="col-sm-12 col-md-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="bootstrap-data-table_paginate"> 
						<?php echo $page_links; ?> 
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="card" id="stockform-card">
            <div class="card-header"> <strong class="card-title stock-title"><?php echo fb_text("add_fish_stock"); ?></strong> </div>
             
            <div class="card-body">
              <form name="stock-form" id="stock-form" method="post" action="<?php echo base_url('stock/create');?>">
                <?php if($this->session->flashdata('success')) {
            ?>
                <div class="sufee-alert alert with-close alert-success alert-dismissible fade show"> <?php echo $this->session->flashdata('success');  ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <?php } ?>
                <?php if($this->session->flashdata('failed')) {
            ?>
                <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert"> <?php echo $this->session->flashdata('failed');  ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <?php } ?>
				 <?php if($this->session->flashdata('update_success')) {
           		 ?>
                <div class="sufee-alert alert with-close alert-success alert-dismissible fade show"> <?php echo $this->session->flashdata('update_success');  ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <?php } ?>
                <?php if($this->session->flashdata('update_failed')) {
          	  ?>
                <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert"> <?php echo $this->session->flashdata('update_failed');  ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <?php } ?>                              
              <form name="stock-form" id="stock-form" method="post" action="<?php echo base_url('stock/create');?>">  
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="pondname"><?php echo fb_text("pond_name"); ?><span class="error">*</span></label>
                    <?php
						$table_name = "ponds";
						$pond_list = fb_combo_arr($table_name);
					?>
                    <select name="pondname" id="pondname" class="form-control">
                      <option selected value=""><?php echo fb_text("choose"); ?></option>
                      <?php foreach($pond_list as $k => $v){ ?>
                      <option value='<?php echo $k?>'><?php echo $v?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="count">Count <span class="error">*</span></label>
                    <input type="text" class="form-control" name="count" id="count" placeholder="<?php echo fb_text("count"); ?>">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="species_type"><?php echo fb_text("species_type"); ?><span class="error">*</span></label>
                      <?php
						$table_name = "species";
						$species_list = fb_combo_arr($table_name);
					?>
                     <select name="species_type" id="species_type" class="form-control">
                      <option selected value=""><?php echo fb_text("choose"); ?></option>
                      <?php foreach($species_list as $k => $v){ ?>
                      <option value='<?php echo $k?>'><?php echo $v?></option>
                      <?php } ?>
                    </select>

                  </div>
                  <div class="form-group col-md-6">
                    <label for="weight"><?php echo fb_text("weight_in_grams"); ?><span class="error">*</span></label>
                    <input type="text" class="form-control" name="weight" id="weight" placeholder="<?php echo fb_text("weight"); ?>">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="fertilizer"><?php echo fb_text("fertilizer"); ?> <span class="error">*</span></label>
                    <input type="text" class="form-control" name="fertilizer" id="fertilizer" placeholder="<?php echo fb_text("fertilizer"); ?>">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="water_type"><?php echo fb_text("water_type"); ?><span class="error">*</span></label>
                    <select name="water_type" id="water_type" class="form-control">
                      <option selected value=""><?php echo fb_text("choose"); ?></option>
                      <option>Sea</option>
                      <option>Fresh</option>
                    </select>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="stock_date"><?php echo fb_text("date"); ?><span class="error">*</span></label>
                    <div class="input-group date" id="datetimepicker" data-target-input="nearest">
                      <input type="text" class="form-control datetimepicker-input" name="date" id="stock_date" data-target="#datetimepicker"/>
                      <div class="input-group-append datetimepicker-icon" data-target="#datetimepicker" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-secondary cancel">Clear</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .animated --> 
  </div>
</div>
<!-- /#right-panel --> 

<!-- Right Panel --> <!-- Delete Modal --> 
<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticModalLabel">Delete</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p>
               Are you sure to delete this?
            </p>
        </div>
        <div class="modal-footer">
        	<form method="post"  action="<?php echo base_url('stock/delete');?>">
            <input type="hidden" name="rid" id="delete_rid"/>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Confirm</button>
            </form>
        </div>
    </div>
</div>
</div>



<!-- View Modal --> 
<div class="modal fade" id="view-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="distributionmodel"><?php echo fb_text("fish_stock"); ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p><label><?php echo fb_text("pond_name"); ?>:</label> &nbsp;&nbsp; <span id="pondnamemodel"></span></p>
            <p><label><?php echo fb_text("count"); ?>:</label>&nbsp;&nbsp; <span id="countmodel"></span></p>
           <p><label><?php echo fb_text("species_type"); ?>:</label> &nbsp;&nbsp; <span id="speciestypemodel"></span></p>
           <p><label><?php echo fb_text("weight"); ?>:</label> &nbsp;&nbsp; <span id="weightmodel"></span></p>
           <p><label><?php echo fb_text("fertilizer"); ?>:</label> &nbsp;&nbsp; <span id="fertilizermodel"></span></p>
           <p><label><?php echo fb_text("water_type"); ?>:</label> &nbsp;&nbsp; <span id="watertypemodel"></span></p>
           <p><label><?php echo fb_text("date"); ?>:</label> &nbsp;&nbsp; <span id="stockeddatemodel"></span></p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>