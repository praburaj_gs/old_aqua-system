<div id="right-panel" class="right-panel">
  <?php $this->load->view('./include/top_menu'); ?>
  <div class="breadcrumbs">
    <div class="col-sm-4">
      <div class="page-header float-left">
        <div class="page-title">
          <h1>Stocks</h1>
        </div>
      </div>
    </div>
    <div class="col-sm-8">
      <div class="page-header float-right">
        <div class="page-title">
          <ol class="breadcrumb text-right">
            <li><a href="#"><?php echo fb_text("dashboard"); ?></a></li>
            <li class="active"><?php echo fb_text("current_stock"); ?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="content mt-3">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header"> <strong class="card-title"><?php echo fb_text("current_stock"); ?></strong> </div>
            <div class="card-body">
			<?php if($this->session->flashdata('success')) {
            ?>
                <div class="sufee-alert alert with-close alert-success alert-dismissible fade show"> <?php echo $this->session->flashdata('success');  ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <?php } ?>
                <?php if($this->session->flashdata('failed')) {
            ?>
                <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert"> <?php echo $this->session->flashdata('failed');  ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <?php } ?>
             <?php if($this->session->flashdata('delete_success')) {
			  ?>
			  <div class="sufee-alert alert with-close alert-success alert-dismissible fade show"> <?php echo $this->session->flashdata('delete_success');  ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <?php } ?>
			  <?php if($this->session->flashdata('delete_failed')) {
			  ?>
			  <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert"> <?php echo $this->session->flashdata('delete_failed');  ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <?php } ?>       
              <div id="bootstrap-data-table_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                <div class="row">
                  <div class="col-sm-12 col-md-6">
                    <div class="dataTables_length" id="bootstrap-data-table_length">
                      <label> Show
                        <select name="per_page" aria-controls="bootstrap-data-table" class="form-control form-control-sm">
                          <option value="10">10</option>
                          <option value="20">20</option>
                          <option value="50">50</option>
                          <option value="-1">All</option>
                        </select>
                        entries </label>
                    </div>
                  </div>
                  <div class="col-sm-12 col-md-6">
                    <div id="bootstrap-data-table_filter" class="dataTables_filter float-right">
                      <label>Search:
                        <input type="search" name="search" class="form-control form-control-sm" placeholder="" >
                      </label>
                    </div>
                    <input type="hidden" name="sort_fld" value="">
                    <input type="hidden" name="sort_dir" value="">
                  </div>
                </div>
                <div class="row"> 
                  <div class="col-sm-12">           
					 <table class="table table-striped table-bordered no-footer" role="grid">
						<thead>
						   <tr role="row">
							  <th><?php echo fb_text("pond_name"); ?></th>
                              <th><?php echo fb_text("count"); ?></th>
							  <th><?php echo fb_text("species_type"); ?></th>
                              <th><?php echo fb_text("modified"); ?></th>
							  <th><?php echo fb_text("action"); ?></th>
						   </tr>
						</thead>
						<tbody>
						   
						   <?php foreach($result_set as $row): 
							  $source = $row["_source"];
							  $rkey = $row["_id"];
						   ?>
						     <tr role="row">
							  <td class=""><?php echo $source["pondname"]; ?></td>
                              <td><?php echo $source["count"]; ?></td>
							  <td><?php echo $source["species_type"]; ?></td>
							  <td class=""><?php echo fb_convert_date($source["updatedtime"]); ?></td>
							  <td><a href="#" data-id="" class="view-modal" data-toggle="modal" data-target="#view-modal"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; 
							  <a href="#" data-id="<?php echo $rkey; ?>" data-pname="<?php echo $source["pondname"]; ?>"  data-stype="<?php echo $source["species_type"]; ?>" class="sampling" data-toggle="modal" data-target="#sampling"><i class="fa fa-tasks"></i></a>&nbsp;&nbsp;
							  <!--<a href="#" data-id="<?=$rkey?>" class="delete-modal" data-toggle="modal" data-target="#delete-modal"><i class="fa fa-trash"></i></a>--></td>
						    </tr>
						   <?php endforeach; ?>
						</tbody>
					 </table>
				  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 col-md-5"> 
                    <!--<div class="dataTables_info" id="bootstrap-data-table_info" role="status" aria-live="polite">Showing 11 to 20 of 25 entries</div>--> 
                  </div>
                  <div class="col-sm-12 col-md-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="bootstrap-data-table_paginate"> 
						<?php echo $page_links; ?> 
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
       
	</div>
    <!-- .animated --> 
  </div>
</div>
<!-- /#right-panel --> 

<!-- Right Panel --> <!-- Delete Modal --> 
<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticModalLabel">Delete</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p>
               Are you sure to delete this?
            </p>
        </div>
        <div class="modal-footer">
        	<form method="post"  action="<?php echo base_url('stock/delete');?>">
            <input type="hidden" name="rid" id="delete_rid"/>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Confirm</button>
            </form>
        </div>
    </div>
</div>
</div>
<!-- view modal -->
<div class="modal fade" id="view-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="pondnamemodel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p><label><?php echo fb_text("count"); ?> :</label> &nbsp;&nbsp; <span id="countcsmodel"></span></p>
            <p><label><?php echo fb_text("species_type"); ?>:</label>&nbsp;&nbsp; <span id="speciestypemodel"></span></p>
			<p><label><?php echo fb_text("modified"); ?> :</label>&nbsp;&nbsp; <span id="modifiedmodel"></span></p>
			
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>


<!--sampling Form-->
<div class="modal fade" id="sampling" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<form name="sampling" id="samplinglist-form" method="post" action="<?php echo base_url('sampling/add_record');?>">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="pondnameLabel"></h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
				<div class="modal-loader">
						<input type="hidden" class="form-control" name="pondname" id="spondnamemodel"  />
						<input type="hidden" class="form-control" name="species_type" id="sspeciestypemodel" />
					<div class="form-group">
					<div class="form-group col-md-12">
					<label><?php echo fb_text("fish_count"); ?>:</label>
						<input type="text" id="countmodel" name="count" class="form-control" placeholder="<?php echo fb_text("fish_count"); ?>"/>
					</div>
					</div>
					<div class="form-group">
					<div class="form-group col-md-12">
					<label><?php echo fb_text("avg_weight"); ?>:</label>
						<input type="text" id="avgweightmodel" name="avg_weight" class="form-control" placeholder="<?php echo fb_text("avg_weight"); ?>"/>
					</div>
					</div>
					<div class="form-group">
					<div class="form-group col-md-12">
					<label><?php echo fb_text("fish_length"); ?>:</label>
						<input type="text" id="fishlengthmodel" name="fish_length" class="form-control" placeholder="<?php echo fb_text("fish_length"); ?>"/>
					</div>
					</div>
					<div class="form-group">
					<div class="form-group col-md-12">
					<label><?php echo fb_text("date"); ?>:</label>
					<div class="input-group date" id="datetimepicker" data-target-input="nearest">
						<input type="text" class="form-control datetimepicker-input" id="samplingdatemodel" name="sampling_date" data-target="#datetimepicker"/>
					  <div class="input-group-append datetimepicker-icon" data-target="#datetimepicker" data-toggle="datetimepicker">
						<div class="input-group-text"><i class="fa fa-calendar"></i></div>
					  </div>
					</div>
					</div>
					</div>
					<div class="form-group">
					<div class="form-group col-md-12">
					<label><?php echo fb_text("observation"); ?>:</label>
						<textarea id="observationmodel" class="form-control"  name="observation" placeholder="<?php echo fb_text("observation"); ?>"></textarea>
					</div>
					</div>
				</div></div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Save</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				</div>
				
			</div>
		</form>
	</div>
</div>
