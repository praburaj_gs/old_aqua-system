<div class="card-header"> <strong class="card-title"><?php echo fb_text("edit_species"); ?></strong> </div>
<div class="card-body">
  <form name="species" id="species-form" method="post" action="<?php echo base_url('species/update');?>">
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="species_type"><?php echo fb_text("species_name"); ?><span class="error">*</span></label>
        <input type="text" class="form-control" name="speciestype" id="speciestype" placeholder="<?php echo fb_text("species_type"); ?>" value="<?= $record['species_type']; ?>">
      </div>
	  <div class="form-group col-md-6">
        <label for="feedname"><?php echo fb_text("feed_name"); ?><span class="error">*</span></label>
         <?php
						$table_name = "feeds";
						 $no_id=true;
						$feedname = fb_combo_arr($table_name, $no_id);						
					?>
          <select name="feedname" id="feedname" class="form-control">
            <option selected value=""><?php echo fb_text("choose"); ?></option>
            <?php foreach($feedname as $k => $v){ ?>
            <option value='<?php echo $k; ?>' <?php if($record['feedname']==$k){ ?> selected="selected" <?php } ?>><?php echo $v?></option>
            <?php } ?>
			</select>
      </div>
      
      <div class="form-group col-md-6">
        <label for="max_weight"><?php echo fb_text("market_size"); ?><span class="error">*</span></label>
        <input type="text" class="form-control" name="maxweight" id="max_weight" placeholder="<?php echo fb_text("size"); ?>" value="<?= $record['max_weight']; ?>">
      </div>
     </div>
    
    <input type="hidden" name="rkey" value="<?=$rkey?>">
    <button type="submit" class="btn btn-primary">Save</button>
    <button type="button" class="btn btn-secondary cancel">Clear</button>
  </form>
</div>
<script type="application/javascript">

    $.validator.addMethod("chkduplicate", function(value, element, arg) {
		console.log(value);
		var pms = JSON.parse(arg);
		pms["search"] = value ;
		var rkey = $(element).closest("form").find("input[name=rkey]").val() || "";
		pms["rkey"] = rkey;
		var chk_url = site_url + "common/check_duplicate";
		var flg = false;
		$.ajax({
		    	type:"GET",
		    	data: pms,
		    	url: chk_url,
				async:false,
		}).done(function(resp){
			var rdata = JSON.parse(resp);
			console.log(resp);
			console.log(rdata.status);
			if(rdata.status == "success"){
				console.log("success");
				flg = true;
			} else{
				console.log("fail");
				flg = false;
			}
			
		}).fail(function(err){
			console.log(err);
		});
		console.log("123>>>>>>");
		//console.log(pms);
		return flg;
    }, "This field value already exists.");
	
	 $("#species-form").validate({
		rules: {
			feedname: {
				required: true,
			},
			speciestype:{
				required: true,
				chkduplicate: '{ "table_name": "species", "fld_name": "species_type"}'
			},
			maxweight:{
				required: true,
				number: true,
				min :0
			}
		},
		messages: {
			feedname: {
				required: disp_text("err_fname"),
			},
			speciestype: {
				required: disp_text("err_stype"),
				chkduplicate: disp_text("err_stype_exists")
			},
			maxweight: {
				required: disp_text("err_maxweight"),
				number : disp_text("err_num"),
				min : disp_text("err_gt0")
			}
		}
	});
	$(document).on('click','.cancel', function(){
		$("#species-form").find('input, select, textarea').val('');
	});
</script>	