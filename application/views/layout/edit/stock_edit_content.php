 <div class="card-header"> <strong class="card-title stock-title"><?php echo fb_text("edit_fish_stock"); ?></strong> </div>
  <div class="card-body">
	<form name="stock-form" id="stock-form" method="post" action="<?php echo base_url('stock/updateStock');?>">
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="pondname"><?php echo fb_text("pond_name"); ?><span class="error">*</span></label>
          <?php
						$table_name = "ponds";
						$pond_list = fb_combo_arr($table_name);
					?>
          <select name="pondname" id="pondname" class="form-control">
            <option selected value=""><?php echo fb_text("choose"); ?></option>
            <?php foreach($pond_list as $k => $v){ ?>
            <option value='<?php echo $k; ?>' <?php if($record['pondname']==$k){ ?> selected="selected" <?php } ?>><?php echo $v?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group col-md-6">
          <label for="count"><?php echo fb_text("count"); ?><span class="error">*</span></label>
          <input type="text" class="form-control" name="count" id="count" placeholder="<?php echo fb_text("count"); ?>" value="<?= $record['count']; ?>">
        </div>
        <div class="form-group col-md-6">
          <label for="species_type"><?php echo fb_text("species_type"); ?><span class="error">*</span></label>
          <?php
						$table_name = "species";
						$species_list = fb_combo_arr($table_name);
					?>
          <select name="species_type" id="species_type" class="form-control">
            <option selected value=""><?php echo fb_text("choose"); ?></option>
            <?php foreach($species_list as $k => $v){ ?>
            <option value='<?php echo $k?>' <?php if($record['species_type']==$k){ ?> selected="selected" <?php } ?>><?php echo $v?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group col-md-6">
          <label for="weight"><?php echo fb_text("weight_in_grams"); ?><span class="error">*</span></label>
          <input type="text" class="form-control" name="weight" id="weight" placeholder="<?php echo fb_text("weight"); ?>" value="<?= $record['weight']; ?>">
        </div>
        <div class="form-group col-md-6">
          <label for="fertilizer"><?php echo fb_text("fertilizer"); ?><span class="error">*</span></label>
          <input type="text" class="form-control" name="fertilizer" id="fertilizer" placeholder="<?php echo fb_text("fertilizer"); ?>" value="<?= $record['fertilizer']; ?>">
        </div>
        <div class="form-group col-md-6">
          <label for="water_type"><?php echo fb_text("water_type"); ?><span class="error">*</span></label>
          <select name="water_type" id="water_type" class="form-control">
            <option selected value=""><?php echo fb_text("choose"); ?></option>
            <option value="Sea" <?php if($record['water_type']=="Sea"){ ?> selected="selected" <?php } ?>>Sea</option>
            <option value="Fresh" <?php if($record['water_type']=="Fresh"){ ?> selected="selected" <?php } ?>>Fresh</option>
          </select>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="stock_date"><?php echo fb_text("date"); ?><span class="error">*</span></label>
          <div class="input-group date" id="datetimepicker" data-target-input="nearest">
            <input type="text" class="form-control datetimepicker-input" name="date" id="stock_date" value="<?php echo $record["_month"]."/".$record["_day"]."/".$record["_year"]; ?>" data-target="#datetimepicker"/>
            <div class="input-group-append datetimepicker-icon" data-target="#datetimepicker" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
            </div>
          </div>
        </div>
      </div>
      <input type="hidden" name="rkey" value="<?=$rkey?>">
      <button type="submit" class="btn btn-primary">Save</button>
      <button type="button" class="btn btn-secondary cancel">Clear</button>
    </form>
  </div>
</div>
<script type="application/javascript">
$(document).ready(function(e) {
      $("#stock-form").validate({
	  rules: {
		  pondname: {
			  required: true,
		  },
		  count: {
			  required: true,
			  number: true,
			  min:0
		  },
		  species_type: {
			  required: true,
		  },
		  weight: {
			  required: true,
			  number: true,
			  min:0
		  },
		  fertilizer: {
			  required: true,
		  },
		  water_type: {
			  required: true,
		  },
		  date : {
			  required : true
		  }
	  },
	  messages: {

		  pondname: {
			  required: disp_text("err_pname"),
		  },
		  count: {
			  required: disp_text("err_count"),
			  number : disp_text("err_num"),
			  min : disp_text("err_gt0")
		  },
		  species_type: {
			  required: disp_text("err_stype"),
		  },
		  weight: {
			  required: disp_text("err_weight"),
			  number : disp_text("err_num"),
			  min : disp_text("err_gt0")
		  },
		  fertilizer: {
			  required: disp_text("err_fertilizer"),
		  },
		  water_type: {
			  required: disp_text("err_water_type"),
		  },
		  date : {
			  required : disp_text("err_date"),
		  }
  },
  errorPlacement: function(error, element) {
	  
	  if (element.attr("name") == "date" )
		  error.insertAfter(".datetimepicker-icon");
	  else
		  error.insertAfter(element);
	  }

  });	
	if($('#datetimepicker').length>0){
	  $('#datetimepicker').datetimepicker({
		  format: 'L',
		  keepOpen : false
	   });
	}
	$(document).on('click','.cancel', function(){
		$("#stock-form").find('input, select, textarea').val('');
	});
});

</script>	