<div class="card-header"> <strong class="card-title"><?php echo fb_text("edit_pond"); ?></strong> </div>
<div class="card-body">
  <form name="pondlist" id="pondlist-form" method="post" action="<?php echo base_url('pondlist/update');?>">
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="pondname"><?php echo fb_text("pond_name"); ?><span class="error">*</span></label>
        <input type="text" class="form-control" name="pondname" id="pondname" placeholder="<?php echo fb_text("pond_name"); ?>" value="<?= $record['pondname']; ?>">
      </div>
      <div class="form-group col-md-6">
        <label for="Width"><?php echo fb_text("width_in_feet"); ?><span class="error">*</span></label>
        <input type="text" class="form-control" name="width" id="width" placeholder="<?php echo fb_text("width"); ?>" value="<?= $record['width']; ?>">
      </div>
      <div class="form-group col-md-6">
        <label for="length"><?php echo fb_text("length_in_feet"); ?> <span class="error">*</span></label>
        <input type="text" class="form-control" name="length" id="length" placeholder="<?php echo fb_text("length"); ?>" value="<?= $record['length']; ?>">
      </div>
      <div class="form-group col-md-6">
        <label for="Depth"><?php echo fb_text("depth_in_feet"); ?><span class="error">*</span></label>
        <input type="text" class="form-control" name="depth" id="depth" placeholder="<?php echo fb_text("depth"); ?>" value="<?= $record['depth']; ?>">
      </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="inputCity"><?php echo fb_text("built_date"); ?><span class="error">*</span></label>
        <div class="input-group date" id="datetimepicker" data-target-input="nearest">
          <input type="text" class="form-control datetimepicker-input" name="built_date" value="<?php echo fb_convert_date($record["built_date"]); ?>" data-target="#datetimepicker"/>
          <div class="input-group-append datetimepicker-icon" data-target="#datetimepicker" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
      </div>
    </div>
    <input type="hidden" name="rkey" value="<?=$rkey?>">
    <button type="submit" class="btn btn-primary">Save</button>
    <button type="button" class="btn btn-secondary cancel">Clear</button>
  </form>
</div>
<script type="application/javascript">

    $.validator.addMethod("chkduplicate", function(value, element, arg) {
		console.log(value);
		var pms = JSON.parse(arg);
		pms["search"] = value ;
		var rkey = $(element).closest("form").find("input[name=rkey]").val() || "";
		pms["rkey"] = rkey;
		var chk_url = site_url + "common/check_duplicate";
		var flg = false;
		$.ajax({
		    	type:"GET",
		    	data: pms,
		    	url: chk_url,
				async:false,
		}).done(function(resp){
			var rdata = JSON.parse(resp);
			console.log(resp);
			console.log(rdata.status);
			if(rdata.status == "success"){
				console.log("success");
				flg = true;
			} else{
				console.log("fail");
				flg = false;
			}
			
		}).fail(function(err){
			console.log(err);
		});
		console.log("123>>>>>>");
		//console.log(pms);
		return flg;
    }, "This field value already exists.");
	
	$("#pondlist-form").validate({
		rules: {
			pondname: {
				required: true,
				chkduplicate: '{ "table_name": "ponds", "fld_name": "pondname"}'
			},
			width: {
				required: true,
				number: true,
				min:0
			},
			length: {
				required: true,
				number: true,
				min:0
			},
			depth: {
				required: true,
				number: true,
				min:0
			},
			built_date: {
				required: true,
			},
		},
		messages: {

			pondname: {
				required: disp_text("err_pname"),
				chkduplicate: disp_text("err_pname_exists")
			},
			width: {
				required: disp_text("err_pwidth"),
				number : disp_text("err_num"),
				min : disp_text("err_gt0")
			},
			length: {
				required: disp_text("err_plength"),
				number : disp_text("err_num"),
				min : disp_text("err_gt0")
			},
			depth: {
				required: disp_text("err_pdepth"),
				number : disp_text("err_num"),
				min : disp_text("err_gt0")
			},
			built_date: {
				required: disp_text("err_date"),
			}
	},
	errorPlacement: function(error, element) {
		
		if (element.attr("name") == "built_date" )
			error.insertAfter(".datetimepicker-icon");
		else
			error.insertAfter(element);
		}
	});
	
	if($('#datetimepicker').length>0){
	  $('#datetimepicker').datetimepicker({
		  format: 'L',
		  keepOpen : false
	   });
	}
	
	$(document).on('click','.cancel', function(){
		$("#pondlist-form").find('input, select, textarea').val('');
	});
</script>	