<div id="right-panel" class="right-panel">

<?php $this->load->view('./include/top_menu'); ?>


  <div class="breadcrumbs">
    <div class="col-sm-4">
      <div class="page-header float-left">
        <div class="page-title">
          <h1><?php echo fb_text("mortality"); ?></h1>
        </div>
      </div>
    </div>
    <div class="col-sm-8">
      <div class="page-header float-right">
        <div class="page-title">
          <ol class="breadcrumb text-right">
            <li><a href="#"><?php echo fb_text("dashboard"); ?></a></li>
            <li class="active"><?php echo fb_text("mortality"); ?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  
  <div class="content mt-3">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header"> <strong class="card-title"><?php echo fb_text("mortality"); ?></strong> </div>
            <div class="card-body">
             <?php if($this->session->flashdata('delete_success')) {
			  ?>
			  <div class="sufee-alert alert with-close alert-success alert-dismissible fade show"> <?php echo $this->session->flashdata('delete_success');  ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <?php } ?>
			  <?php if($this->session->flashdata('delete_failed')) {
			  ?>
			  <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert"> <?php echo $this->session->flashdata('delete_failed');  ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <?php } ?>            
              <table id="bootstrap-data-table" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th><?php echo fb_text("pond_name"); ?></th>
                    <th><?php echo fb_text("species_type"); ?></th>
                    <th><?php echo fb_text("weight"); ?></th>
                    <th><?php echo fb_text("count"); ?></th>
                    <th><?php echo fb_text("reason"); ?></th>
                    <th><?php echo fb_text("date"); ?></th>
                    <th><?php echo fb_text("action"); ?></th>
                  </tr>
                </thead>
                <tbody>
				<?php foreach($result_set as $row): 
                  $source = $row["_source"];
				  $rkey = $row["_id"];
               ?>
                 <tr role="row">
                  <td class=""><?php echo $source["pondname"]; ?></td>
                  <td><?php echo $source["species_type"]; ?></td>
                  <td><?php echo $source["weight"]; ?></td>                  
                  <td><?php echo $source["count"]; ?></td>
                  <td><?php echo $source["reason"]; ?></td>
                  <td class=""><?php echo fb_convert_date($source['mortality_date']); ?></td>
                  <td><a href="#" data-id="" class="view-modal" data-toggle="modal" data-target="#view-modal"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#" data-id="<?=$rkey?>" class="delete-modal" data-toggle="modal" data-target="#delete-modal"><i class="fa fa-trash"></i></a></td>
                </tr>
               <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="card">
            <div class="card-header"> <strong class="card-title"><?php echo fb_text("add_mortality"); ?></strong> </div>
            <div class="card-body">
              <form id="mortality-form" name="mortality-form" method="post" action="<?php echo base_url('mortality/create');?>">
				<?php if($this->session->flashdata('success')) {
                ?>
                <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">                       
                <?php echo $this->session->flashdata('success');  ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div> 
                <?php } ?>                 
                
                <?php if($this->session->flashdata('failed')) {
                ?>
                <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert">
                <?php echo $this->session->flashdata('failed');  ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div> 
                <?php } ?>               
                <div class="form-row">
                  <div class="form-group col-md-6">
                     <label for="pondname"><?php echo fb_text("pond_name"); ?><span class="error">*</span></label>
                     <?php
						$table_name = "ponds";
						$pond_list = fb_combo_arr($table_name);
					?>
                     <select id="pondname" name="pondname" class="form-control">
                      <option selected value=""><?php echo fb_text("choose"); ?></option>
                      <?php foreach($pond_list as $k => $v){ ?>
                      <option value='<?php echo $k?>'><?php echo $v?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                   <label for="species_type"><?php echo fb_text("species_type"); ?><span class="error">*</span></label>
                    <?php
						$table_name = "species";
						$species_list = fb_combo_arr($table_name);
					?>
                    <select id="species_type" name="species_type" class="form-control">
                      <option selected value=""><?php echo fb_text("choose"); ?></option>
                      <?php foreach($species_list as $k => $v){ ?>
                      <option value='<?php echo $k?>'><?php echo $v?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="weight"><?php echo fb_text("weight"); ?><span class="error">*</span></label>
                    <input type="text" class="form-control" id="weight" name="weight" placeholder="<?php echo fb_text("weight"); ?>">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="count"><?php echo fb_text("count"); ?><span class="error">*</span></label>
                    <input type="text" class="form-control" id="count" name="count" placeholder="<?php echo fb_text("count"); ?>">
                  </div>
                </div>
                <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="reason"><?php echo fb_text("reason"); ?> <span class="error">*</span></label>
                    <input type="text" class="form-control" id="reason" name="reason" placeholder="<?php echo fb_text("reason"); ?>">
                  </div>
                  
                  <div class="form-group col-md-6">
                    <label for="inputCity"><?php echo fb_text("date"); ?><span class="error">*</span></label>
                    <div class="input-group date" id="datetimepicker" data-target-input="nearest">
                      <input type="text" class="form-control datetimepicker-input" name="date" data-target="#datetimepicker"/>
                      <div class="input-group-append datetimepicker-icon" data-target="#datetimepicker" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-secondary cancel">Cancel</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .animated --> 
  </div>
  



</div><!-- /#right-panel -->

<!-- Right Panel -->

<!-- Delete Modal --> 
<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticModalLabel">Delete</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p>
               Are you sure to delete this?
            </p>
        </div>
        <div class="modal-footer">
        	<form method="post"  action="<?php echo base_url('mortality/delete');?>">
            <input type="hidden" name="rid" id="delete_rid"/>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Confirm</button>
            </form>
        </div>
    </div>
</div>
</div>


<!-- View Modal --> 
<div class="modal fade" id="view-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="mortalitymodel"><?php echo fb_text("mortality"); ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p><label><?php echo fb_text("pond_name"); ?>:</label> &nbsp;&nbsp; <span id="pondnamemodel"></span></p>
           <p><label><?php echo fb_text("species_type"); ?>:</label> &nbsp;&nbsp; <span id="speciestypemodel"></span></p>
           <p><label><?php echo fb_text("weight"); ?>:</label> &nbsp;&nbsp; <span id="weightmodel"></span></p>
           <p><label><?php echo fb_text("count"); ?>:</label> &nbsp;&nbsp; <span id="countmodel"></span></p>
           <p><label><?php echo fb_text("reason"); ?>:</label> &nbsp;&nbsp; <span id="reasonmodel"></span></p>
           <p><label><?php echo fb_text("date"); ?>:</label> &nbsp;&nbsp; <span id="moralitydatemodel"></span></p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>