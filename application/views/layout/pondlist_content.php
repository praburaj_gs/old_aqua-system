<div id="right-panel" class="right-panel">

<?php $this->load->view('./include/top_menu'); ?>

<div class="breadcrumbs">
<div class="col-sm-4">
  <div class="page-header float-left">
    <div class="page-title">
      <h1><?php echo fb_text("pond_list"); ?></h1>
    </div>
  </div>
</div>
<div class="col-sm-8">
  <div class="page-header float-right">
    <div class="page-title">
      <ol class="breadcrumb text-right">
        <li><a href="#"><?php echo fb_text("dashboard"); ?></a></li>
        <li class="active"><?php echo fb_text("pond_list"); ?></li>
      </ol>
    </div>
  </div>
</div>
</div>

<div class="content mt-3">
<div class="animated fadeIn">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header"> <strong class="card-title"><?php echo fb_text("pond_list"); ?></strong> </div>
        <div class="card-body">
             <?php if($this->session->flashdata('delete_success')) {
			  ?>
			  <div class="sufee-alert alert with-close alert-success alert-dismissible fade show"> <?php echo $this->session->flashdata('delete_success');  ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <?php } ?>
			  <?php if($this->session->flashdata('delete_failed')) {
			  ?>
			  <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert"> <?php echo $this->session->flashdata('delete_failed');  ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <?php } ?>         
          <div id="bootstrap-data-table_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
          			<form name="search" id="srchfrm" action="<?php echo site_url("/pondlist/"); ?>" method="GET">			 
				   <div class="row">
					  <div class="col-sm-12 col-md-6">
						 <div class="dataTables_length" id="bootstrap-data-table_length">
							<label>
							   <?php echo fb_text("show"); ?> 
							   <select name="no_items" aria-controls="bootstrap-data-table" class="form-control form-control-sm">
								  <option value="10">10</option>
								  <option value="20">20</option>
								  <option value="50">50</option>
								  <option value="-1">All</option>
							   </select>
							   <?php echo fb_text("entries"); ?> 
							</label>
						 </div>
					  </div>
					  <div class="col-sm-12 col-md-6">
					   <div id="bootstrap-data-table_filter" class="dataTables_filter"><label><?php echo fb_text("search").":"; ?>
						   <input type="search" name="search" class="form-control form-control-sm" placeholder="" value="<?php echo $search; ?>" />
						</label></div>
                         <input type="hidden" name="sort_fld" value="<?php echo $sort_fld; ?>" />
						 <input type="hidden" name="sort_dir" value="<?php echo $sort_dir; ?>" />
					  </div>
				   </div>
                   </form>
							   
			   <div class="row">
				  <div class="col-sm-12">
					 <table class="table table-striped table-bordered no-footer" role="grid">
						<thead>
						   <tr role="row">
							  <?php echo $theader; ?>
						   </tr>
						</thead>
						<tbody>

						   
						   <?php foreach($result_set as $key=>$row): 
							  $source = $row["_source"];
							  $rkey = $row["_id"];
						   ?>
						     <tr role="row">
							  <td class=""><?php echo $source["pondname"]; ?></td>
							  <td><?php echo $source["length"]; ?></td>
							  <td><?php echo $source["width"]; ?></td>
							  <td><?php echo $source["depth"]; ?></td>
							  <td class=""><?php echo fb_convert_date($source["built_date"]); ?></td>
							  <td><a href="#" data-id="" class="view-modal" data-toggle="modal" data-target="#view-modal"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="javascript:void(0);" data-id="<?=$rkey?>" class="edit-pond" ><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#" data-id="<?=$rkey?>" class="delete-modal" data-toggle="modal" data-target="#delete-modal"><i class="fa fa-trash"></i></a></td>
						    </tr>
						   <?php endforeach; ?>
						</tbody>
					 </table>
				  </div>
			   </div>
			   <div class="row">
				  <div class="col-sm-12 col-md-5">
					 <!--<div class="dataTables_info" id="bootstrap-data-table_info" role="status" aria-live="polite">Showing 11 to 20 of 25 entries</div>-->
				  </div>
				  <div class="col-sm-12 col-md-7">
					 <div class="dataTables_paginate paging_simple_numbers" id="bootstrap-data-table_paginate">
					    <?php echo $page_links; ?>

					 </div>
				  </div>
			   </div>
			</div>
        </div>
      </div>
      
    </div>
    <div class="col-md-12">
      <div class="card" id="pondform-card">
        <div class="card-header"> <strong class="card-title">Add Pond</strong> </div>
        <div class="card-body">
          <form name="pondlist" id="pondlist-form" method="post" action="<?php echo base_url('pondlist/create');?>">
			<?php if($this->session->flashdata('success')) {
            ?>
            <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">                       
            <?php echo $this->session->flashdata('success');  ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div> 
            <?php } ?>                 
            
            <?php if($this->session->flashdata('failed')) {
            ?>
            <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert">
            <?php echo $this->session->flashdata('failed');  ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div> 
            <?php } ?>   
             <?php if($this->session->flashdata('update_success')) {
            ?>
                <div class="sufee-alert alert with-close alert-success alert-dismissible fade show"> <?php echo $this->session->flashdata('update_success');  ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <?php } ?>
                <?php if($this->session->flashdata('update_failed')) {
            ?>
                <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert"> <?php echo $this->session->flashdata('update_failed');  ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <?php } ?>                            
                        
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="pondname"><?php echo fb_text("pond_name"); ?><span class="error">*</span></label>
                <input type="text" class="form-control" name="pondname" id="pondname" placeholder="<?php echo fb_text("pond_name"); ?>">
              </div>
              <div class="form-group col-md-6">
                <label for="Widtht"><?php echo fb_text("width_in_feet"); ?> <span class="error">*</span></label>
                <input type="text" class="form-control" name="width" id="width" placeholder="<?php echo fb_text("width"); ?>">
              </div>
              <div class="form-group col-md-6">
                <label for="Length"><?php echo fb_text("length_in_feet"); ?> <span class="error">*</span></label>
                <input type="text" class="form-control" name="length" id="length" placeholder="<?php echo fb_text("length"); ?>">

              </div>
              <div class="form-group col-md-6">
                <label for="Depth"><?php echo fb_text("depth_in_feet"); ?><span class="error">*</span></label>
                 <input type="text" class="form-control" name="depth" id="depth" placeholder="<?php echo fb_text("depth"); ?>">

              </div>
            </div>
            <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="inputCity"><?php echo fb_text("built_date"); ?><span class="error">*</span></label>
                    <div class="input-group date" id="datetimepicker" data-target-input="nearest">
                      <input type="text" class="form-control datetimepicker-input" name="built_date" data-target="#datetimepicker"/>
                      <div class="input-group-append datetimepicker-icon" data-target="#datetimepicker" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                </div>
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-secondary cancel">Clear</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- .animated --> 
</div>

</div><!-- /#right-panel -->

<!-- Right Panel -->
<!-- Delete Modal --> 
<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticModalLabel">Delete</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p>
               Are you sure to delete this?
            </p>
        </div>
        <div class="modal-footer">
        	<form method="post"  action="<?php echo base_url('pondlist/delete');?>">
            <input type="hidden" name="rid" id="delete_rid"/>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Confirm</button>
            </form>
        </div>
    </div>
</div>
</div>

<!-- View Modal --> 
<div class="modal fade" id="view-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="pondnamemodel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p><label><?php echo fb_text("length"); ?>:</label> &nbsp;&nbsp; <span id="pondlengthmodel"></span></p>
            <p><label><?php echo fb_text("width"); ?>:</label>&nbsp;&nbsp; <span id="pondwidthmodel"></span></p>
           <p><label><?php echo fb_text("depth"); ?>:</label> &nbsp;&nbsp; <span id="ponddepthmodel"></span></p>
           <p><label><?php echo fb_text("built_date"); ?>:</label> &nbsp;&nbsp; <span id="pondbuiltdatemodel"></span></p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>