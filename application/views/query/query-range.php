{
	"query": {
		 "range" : {
			 "createdtime" : {
                "gte": __lowv__,
                "lte": __topv__,
				"boost" : 2.0
            }
		 }
    },
	"size" : __size__,
	"from": __from__,
	"sort": { "updatedtime" : {"order" : "desc"} }
}