<div class="language-select dropdown" id="language-select">
	<a class="dropdown-toggle" href="javascript:void(0);" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
	 <strong>{cur_lang}</strong> <i class="flag-icon {cur_lang_icon}"></i>
	</a>
	<div class="dropdown-menu" aria-labelledby="language" >
	  {lang_items}	
		<div class="dropdown-item">
		 <a href="{lang_url}">{lang_text} <span class="flag-icon {lang_icon}"></span></a>
		</div>
	  {/lang_items}	
	</div>
</div>