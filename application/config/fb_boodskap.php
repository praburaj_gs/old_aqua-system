<?php
/**
 * Name:    Fourbends - Boodskap API
 * Author:  Fourbends Dev Team
 *          contactus@fourbends.com
 * Url:    http://www.fourbends.com/
 *
 *
 * Created:  02.03.2018
 *
 * Description:  This file is used to refer the tables list in boodskap.
 * Original Author name has been kept but that does not mean that the method has not been modified.
 *
 * Requirements: PHP5 or above
 *
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$config['tables'] = array( "ponds" => "1000", "species" => "1001",
                        "fish_stock" => "1002", "distribution" => "1003",
						"harvest" => "1004", "mortality" => "1005",
                        "current_stock" => "1006", "pond_cleaning" => "1007",
                        "feeds" => "1008", "sampling" => "1009", "activities" => "1010",'feed_stock'=>'1011','feed_distribution'=>'1012', "ponds1" => "2000"
                    );


$config["ponds1"] = array("pondname", "length", "width", "depth", "built_date", "createdtime", "updatedtime"); // Testing ponds
					
$config["ponds"] = array("pondname", "length", "width", "depth", "built_date", "createdtime", "updatedtime");

$config["species"] = array("feedname", "max_weight", "species_type", "createdtime", "updatedtime");

$config["fish_stock"] = array("pondname", "pond_id", "species_type", "species_id", "stocked_date", "count", "fertilizer", "water_type", "weight", "_day", "_month", "_year", "createdtime", "updatedtime");

$config["distribution"] = array("from_pond", "from_pond_id", "to_pond", "to_pond_id", "species_type", "species_id", "count", "updatedby", "distribution_date", "createdtime", "updatedtime");

$config["harvest"] = array("pondname", "pond_id", "species_type", "species_id", "count", "weight", "harvest_date", "createdtime", "updatedtime");

$config["mortality"] = array("pondname", "pond_id", "species_type", "species_id", "reason", "count", "weight", "mortality_date", "createdtime", "updatedtime");

$config["current_stock"] = array("pondname", "species_type", "count", "weight", "stocked_date", "_day", "_month", "_year", "createdtime", "updatedtime");

$config["pond_cleaning"] = array("pondname", "pond_id", "cleanedby", "cleaned_date", "createdtime", "updatedtime");

$config["feeds"] = array("feedname", "size", "feed_type", "createdtime", "updatedtime");

$config["sampling"] = array("pondname", "species_type", "count", "fish_length", "avg_weight", "sampling_date", "sampling_state", "observation", "createdtime", "updatedtime");

$config["feed_stock"] = array("feed_name", "stocked_weight", "current_weight" , "stocked_date","createdtime", "updatedtime");

$config["feed_distribution"] = array("pond_name","pond_id","feed_name","feed_stock_id","weight", "date", "createdtime", "updatedtime");

$config["activities"] =  array("activity_module", "activity_fldname", "activity_fldval", "activity_fldid", "activity_type",
 "activity_state", "activity_date", "activity_ref", "activity_action", "createdtime",
 "updatedtime"
 );

$config['combo_list'] = array(
   "ponds1" => array( "key" => "pondname", "value" => "pondname", "sort_fld" => "pondname", "sort_dir" => "asc" ), 
   "ponds" => array( "key" => "_id", "value" => "pondname", "sort_fld" => "pondname", "sort_dir" => "asc" ), 
   "feeds" => array( "key" => "_id", "value" => "feedname", "sort_fld" => "feedname", "sort_dir" => "asc" ),
   "species" => array( "key" => "_id", "value" => "species_type", "sort_fld" => "species_type", "sort_dir" => "asc" ),
   "feed_stock" => array( "key" => "_id", "value" => "feed_name", "sort_fld" => "feed_name", "sort_dir" => "asc" )
);


$config['affect_flds'] = array(
	"ponds1" => "pondname", "ponds" => "pondname", "species" => "species_type", "feeds" => "feedname"
);

$config['affect_list'] = array(
	"ponds1" => array(
       "fish_stock:pondname", "distribution:from_pond", "distribution:to_pond", "harvest:pondname", "mortality:pondname", "current_stock:pondname",
	   "pond_cleaning:pondname", "sampling:pondname"
    ),
	
   "ponds" => array(
       "fish_stock:pondname", "distribution:from_pond", "distribution:to_pond", "harvest:pondname", "mortality:pondname", "current_stock:pondname",
	   "pond_cleaning:pondname", "sampling:pondname"
    ),
   
   "species" => array(
       "fish_stock:species_type", "distribution:species_type", "harvest:species_type", "mortality:species_type", "current_stock:species_type",
	   "sampling:species_type"
    ),
	
	"feeds" => array(
       "species:feedname"
    )
);

$config["language"] = array(
    'en' => array( "disp_txt" => "English", "lang_name" => "english", "lang_key" => "en", "icon_cls" => "flag-icon-us" ),
	'fr' => array( "disp_txt" => "Français", "lang_name" => "french", "lang_key" => "fr", "icon_cls" => "flag-icon-fr" ),
	'es' => array( "disp_txt" => "Español", "lang_name" => "spanish", "lang_key" => "es", "icon_cls" => "flag-icon-es" ),
	'ta' => array( "disp_txt" => "தமிழ்", "lang_name" => "tamil", "lang_key" => "ta", "icon_cls" => "flag-icon-in" ),
	'hi' => array( "disp_txt" => "हिन्दी ", "lang_name" => "hindi", "lang_key" => "hi", "icon_cls" => "flag-icon-in" )
);

$config["timezone"] = "Asia/Kolkata";