<?php
/**
 * Name:    Fourbends Boodskap API
 * Author:  Fourbends Dev Team
 *          contactus@fourbends.com
 * Url:    http://www.fourbends.com/
 *
 *
 * Created:  02.03.2018
 *
 * Description:  This file is used to connect our PHP system to Boodskap API.
 * Original Author name has been kept but that does not mean that the method has not been modified.
 *
 * Requirements: PHP5 or above
 *
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

get_instance()->load->iface('FbsIotRestInterface');

class FB_Boodskap_rest implements FbsIotRestInterface
{

	protected $CI;
    protected $tables;
	protected $tbl_actvy;
	
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->config->load('fb_boodskap', TRUE);
		$this->tables = $this->CI->config->item('tables', 'fb_boodskap');
		$this->tbl_actvy = "activities";
		require_once( APPPATH . 'third_party/boodskap/autoload.php');
	}
	
	public function login($email, $password){
		
		$api_instance = new Swagger\Client\Api\LoginApi();
		
		try {
			$result = $api_instance->login($email, $password);
			
			$user_token = $result->getToken();
			$domain_key = $result->getDomainKey();
			$api_key = $result->getApiKey();
			$cUser = $result->getUser();
			$email = $cUser->getEmail();
			$first_name = $cUser->getFirstName();
			$last_name = $cUser->getLastName();
			$country = $cUser->getCountry();
			$state = $cUser->getState();
			$city = $cUser->getCity();
			$address = $cUser->getAddress();
			$zipcode = $cUser->getZipcode();
			$locale = $cUser->getLocale();
			$timezone = $cUser->getTimezone();
			
			$udata = array(
				"user_token" => $user_token,
				"domain_key" => $domain_key,
				"api_key" => $api_key,
			    "email" => $email,
				"first_name" => $first_name,
				"last_name" => $last_name,
				"country" => $country,
				"state" => $state,
				"city" => $city,
				"address" => $address,
				"zipcode" => $zipcode,
				"locale" => $locale,
				"timezone" => $timezone
			);
			$this->CI->session->set_userdata($udata); // Set the session data
			$msg = array("status" => "success", "message" => "Successfully Loggedin");
			return $msg;
		} catch (Exception $e) {
			$smsg = $e->getMessage();
			log_message('error', "Exception when calling LoginApi->login: ".$smsg);
			$msg = array("status" => "fail", "message" => "Invalid Login");
			return $msg;
		}
	}
	
	public function isloggedin(){
		$status = $this->CI->session->has_userdata("user_token");
		return $status;
	}
	
	public function get_fbuser_data($name){
		$udata = $this->CI->session->has_userdata($name);
		if($udata){
			return $this->CI->session->userdata($name);
		}else{
			return "";
		}
	}
	
	public function create_record($table_name = "", $idata = array(), $activity = false){
		if(!$this->isloggedin()){
			$msg = array("status" => "fail", "message" => "Please login to continue");
			return $msg;
		}
		
		$tcond = (isset($this->tables[$table_name]) && !empty($idata) );
		if($tcond){
			$api_instance = new Swagger\Client\Api\InsertUpdateRecordApi();
			$tbl_id = $this->tables[$table_name]; // table idata
			$user_token = $this->get_fbuser_data("user_token");
			$ndata = $this->fb_setrecord($table_name, $idata);
			$sdata = json_encode($ndata);
			
			try {
				$result = $api_instance->upsertRecord($user_token, $tbl_id, $sdata);
				$ikey = $result->getRkey();
				if(!$activity){
					$this->update_activities($table_name, $ndata, 'add', array() , $ikey); // update the activities
				}
				
				$msg = array("status" => "success", "message" => "Successfully created record", "rkey" => $ikey);
				return $msg;
			} catch (Exception $e) {
				$smsg = $e->getMessage();
				log_message('error', "Exception when calling InsertUpdateRecordApi->upsertRecord: ".$smsg);
				$msg = array("status" => "fail", "message" => "Invalid Table or Data or Boodskap Server issue");
				return $msg;
			}
			
		}else{
			$msg = array("status" => "fail", "message" => "Invalid Table or Data");
			return $msg;
		}
		
	}
	
	public function test_list_record($table_name = ""){
		if(!$this->isloggedin()){
			$msg = array("status" => "fail", "message" => "Please login to continue");
			fb_pr($msg);
			return $msg;
		}
		
		$api_instance = new Swagger\Client\Api\SearchApi();
		$atoken = $this->get_fbuser_data("user_token");
		$tbl_id = $this->tables[$table_name];
		$type = "RECORD";
		$query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
		$queryObj = array();
		//$queryObj['query']= new stdClass();
		//"match_all": {} 
		$queryObj['size'] = 10000;
		$qmain = new stdClass();
		$qmain->match_all = new stdClass();
		$queryObj['query']= $qmain;
		$queryObj['sort'] = array("updatedtime" => array("order" => "desc" )); 
		$qstr = json_encode($queryObj);
		fb_pr($qstr);
		$query['query']= $qstr;
		$query['method']="GET";
		$repositary = "";
		$mapping = "";
		try {
			$sresult = $api_instance->search($atoken, $type, $query, $tbl_id, $repositary, $mapping);
			$result = $sresult->getResult();
			fb_pr($result);
		} catch (Exception $e) {
			echo 'Exception when calling SearchApi->search: ', $e->getMessage(), PHP_EOL;
		}

	}
	
	// Parameters list ("page_no", "per_page", "uri_segment", "search", "sort_fld", "sort_dir", "page_burl", "table_name")
	
	public function list_record($params = array()){
		if(!$this->isloggedin()){
			$msg = array("status" => "fail", "message" => "Please login to continue");
			return $msg;
		}
		
		$pre_params =  array("page_no" => "1", "per_page" => "10", "uri_segment" => "2", "search" => "", "sort_fld" => "updatedtime", "sort_dir" => "desc",
		"page_burl" => site_url("/"), "table_name" => "");
		
		foreach($pre_params as $pk => $pv){
			if(!isset($params[$pk]) || empty($params[$pk])){
				$params[$pk] = $pv;
			}
		}

		extract($params);
		
		$from = ($page_no <= 1) ? "0" : ( $page_no - 1 ) ;
		$from = ($from * $per_page);
		//"from" : 0
		$page_params = compact("per_page", "uri_segment", "page_burl");
		
		$tcond = isset($this->tables[$table_name]);
		if($tcond){
			$api_instance = new Swagger\Client\Api\SearchApi();
			$atoken = $this->get_fbuser_data("user_token");
			$tbl_id = $this->tables[$table_name];
			$type = "RECORD";
			$query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
			$queryObj = array();
			
			$qmain = new stdClass();
			if(!empty($search))
			{
				$search = trim($search, "*")."*";
				$qmain->query_string = new stdClass();
				$qmain->query_string->query = $search;
			}
			
			$queryObj['query']= $qmain;
			$queryObj['size'] = $per_page;
			$queryObj['from'] = $from;
			$queryObj['sort'] = array($sort_fld => array("order" => $sort_dir )); 
			$qstr = json_encode($queryObj);
			$query['query']= $qstr;
			$query['method']="GET";
			$repositary = "";
			$mapping = "";
			try {
				$oresult = $api_instance->search($atoken, $type, $query, $tbl_id, $repositary, $mapping);
				$sresult = $oresult->getResult();
				$aresult = json_decode($sresult, true);
				$total_count = isset($aresult["hits"]["total"]) ? $aresult["hits"]["total"]: "0";
				$result_set = isset($aresult["hits"]["hits"]) ? $aresult["hits"]["hits"] : array();
				$page_params["total_rows"] = $total_count;
				$page_links = fb_generate_pagination($page_params);
				$page_links = ( !empty($result_set) ) ? $page_links : "";
				$msg = array("status" => "success", "message" => "Search result got it",
				"page_links" => $page_links, "result_set" => $result_set);
				return $msg;
			} catch (Exception $e) {
				$smsg = $e->getMessage();
				log_message('error', "Exception when calling SearchApi->search: ".$smsg);
				$msg = array("status" => "fail", "message" => "Search functionality error");
				return $msg;
			}
			
		}else{
			$msg = array("status" => "fail", "message" => "Invalid Table name");
			return $msg;
		}
		
		
	}
	
	private function fb_setrecord($table_name = "", $idata = array()){
		$newdata = array();
		$tblkeys = $this->CI->config->item($table_name, 'fb_boodskap');
		foreach($tblkeys as $tblkey){
			$newdata[$tblkey] = isset($idata[$tblkey]) ? trim($idata[$tblkey]) : "";
		}
		return $newdata;
	}
	
	private function fb_updaterecord($table_name = "", $idata = array(), $odata = array()){
		$newdata = array();
		$tblkeys = $this->CI->config->item($table_name, 'fb_boodskap');
		foreach($tblkeys as $tblkey){
			$newdata[$tblkey] = isset($idata[$tblkey]) ? trim($idata[$tblkey]) : $odata[$tblkey];
		}
		return $newdata;
	}
	
	public function combo_list($table_name = "", $no_id = false){
		if(!$this->isloggedin()){
			$msg = array("status" => "fail", "message" => "Please login to continue");
			return $msg;
		}
		
		$combo_list = $this->CI->config->item('combo_list', 'fb_boodskap');
		if(!isset($combo_list[$table_name])){
			$msg = array("status" => "fail", "message" => "Please give the correct table name");
			return $msg;
		}
		
		$cmb_cfg = $combo_list[$table_name];
		$kfld_val = $cmb_cfg["key"];
		$vfld_val = $cmb_cfg["value"];
		$sfld_val = $cmb_cfg["sort_fld"];
		$sdir_val = $cmb_cfg["sort_dir"];
		$acombo = array();
		
		$api_instance = new Swagger\Client\Api\SearchApi();
		$atoken = $this->get_fbuser_data("user_token");
		$tbl_id = $this->tables[$table_name];
		$type = "RECORD";
		$query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
		$queryObj = array();
		$queryObj['size'] = 10000;
		$qmain = new stdClass();
		$qmain->match_all = new stdClass();
		$queryObj['query']= $qmain;
		$queryObj['sort'] = array($sfld_val => array("order" => $sdir_val )); 
		$qstr = json_encode($queryObj);
		
		$query['query']= $qstr;
		$query['method']="GET";
		$repositary = "";
		$mapping = "";
		try {
			$oresult = $api_instance->search($atoken, $type, $query, $tbl_id, $repositary, $mapping);
			$sresult = $oresult->getResult();
			$aresult = json_decode($sresult, true);
			$total_count = isset($aresult["hits"]["total"]) ? $aresult["hits"]["total"]: "0";
			$result_set = isset($aresult["hits"]["hits"]) ? $aresult["hits"]["hits"] : array();
			foreach($result_set as $row){
				$src = $row["_source"];
				$_id = $row["_id"];
				$k = ($kfld_val=="_id") ? $_id : $src[$kfld_val];
				$k = ($no_id) ? $src[$vfld_val] : $k;
				$v = $src[$vfld_val];
				$acombo[$k] = $v;
			}
			$msg = array("status" => "success", "message" => "Fetch Table: $table_name result successfully", "combo_list" => $acombo);
			return $msg;
		} catch (Exception $e) {
			$smsg = $e->getMessage();
			log_message('error', "Exception when calling SearchApi->search: ".$smsg);
			$msg = array("status" => "fail", "message" => "Search functionality error");
			return $msg;
		}
	}
	
	public function delete_record($table_name = "", $rkey = "", $activity = false){
		if(!$this->isloggedin()){
			$msg = array("status" => "fail", "message" => "Please login to continue");
			return $msg;
		}
		
		$tcond = isset($this->tables[$table_name]);
		if($tcond){
			$api_instance = new Swagger\Client\Api\DeleteRecordApi();
			$tbl_id = $this->tables[$table_name];
			$user_token = $this->get_fbuser_data("user_token");
			try {
				$oresult = $this->get_record($table_name, $rkey);
				$result = $api_instance->deleteRecord($user_token, $tbl_id, $rkey);
				if(!$activity){
					$ddata = $oresult["result_set"];
					$this->update_activities($table_name, $ddata, 'delete', array() , $rkey); // update the activities
				}
				$msg = array("status" => "success", "message" => "Successfully deleted the record");
				return $msg;
			} catch (Exception $e) {
				$smsg = $e->getMessage();
				log_message('error', "Exception when calling DeleteRecordApi->deleteRecord: ".$smsg);
				$msg = array("status" => "fail", "message" => "Server issue or Rkey is invalid.");
				return $msg;
			}
		} else {
			$msg = array("status" => "fail", "message" => "Invalid Table name");
			return $msg;
		}
	}
	
	public function update_record($table_name = "", $idata = array(), $rkey = '', $activity = false){
		if(!$this->isloggedin()){
			$msg = array("status" => "fail", "message" => "Please login to continue");
			return $msg;
		}
		
		$tcond = (isset($this->tables[$table_name]) && !empty($idata) );
		if($tcond){
			// Fetch record
			$gmsg = $this->get_record($table_name, $rkey);
			if($gmsg["status"] == "success")
			{
				$odata = $gmsg["result_set"];
				$api_instance = new Swagger\Client\Api\InsertUpdateRecordWithIDApi();
				$tbl_id = $this->tables[$table_name];
				$user_token = $this->get_fbuser_data("user_token");
				
				// User modified fields only affected.
				$ndata = $this->fb_updaterecord($table_name, $idata, $odata);
				$sdata = json_encode($ndata);
				
				try {
					$result = $api_instance->upsertRecordWithID($user_token, $tbl_id, $rkey, $sdata);
					if(!$activity){
						$this->update_activities($table_name, $ndata, 'update', $odata, $rkey); // update the activities
					}
					
					$msg = array("status" => "success", "message" => "Successfully updated record");
					return $msg;
				} catch (Exception $e) {
					$smsg = $e->getMessage();
					log_message('error', "Exception when calling InsertUpdateRecordWithIDApi->upsertRecordWithID: ".$smsg);
					$msg = array("status" => "fail", "message" => "Invalid Table or Data or Boodskap Server issue");
					return $msg;
				}
			} else {
				return $gmsg;
			}
			
		}else{
			$msg = array("status" => "fail", "message" => "Invalid Table or Data");
			return $msg;
		}
		
	}
	
	public function get_record($table_name = "", $rkey = ""){
		$api_instance = new Swagger\Client\Api\SearchApi();
		$atoken = $this->get_fbuser_data("user_token");
		$tbl_id = $this->tables[$table_name];
		$type = "RECORD";
		$query = new \Swagger\Client\Model\SearchQuery();
		
		$qmain = new stdClass();
		$qmain->terms = new stdClass();
		$qmain->terms->_id = array($rkey);
		$queryObj['query']= $qmain;
		$qstr = json_encode($queryObj);
		
		$query['query']= $qstr;
		$query['method']="GET";
		$repositary = "";
		$mapping = "";
		try {
			$oresult = $api_instance->search($atoken, $type, $query, $tbl_id, $repositary, $mapping);
			$sresult = $oresult->getResult();
			$aresult = json_decode($sresult, true);
			$total_count = isset($aresult["hits"]["total"]) ? $aresult["hits"]["total"]: "0";
			$result_set = isset($aresult["hits"]["hits"]) ? $aresult["hits"]["hits"] : array();
			if(!empty($result_set)){
				list($cresult) = $result_set;
				$csrc = $cresult["_source"];
				$msg = array("status" => "success", "message" => "Successfully fetched result.",
				"result_set" => $csrc);
				return $msg;
			} else {
				$msg = array("status" => "fail", "message" => "Invalid RKEY.");
				return $msg;
			}
		} catch (Exception $e) {
			$smsg = $e->getMessage();
			log_message('error', "Exception when calling SearchApi->search: ".$smsg);
			$msg = array("status" => "fail", "message" => "Search functionality error");
			return $msg;
		}
	}
	
	public function check_duplicate($table_name = "", $fld_name = "", $search = "", $rkey = ""){
		if(!$this->isloggedin()){
			$msg = array("status" => "fail", "message" => "Please login to continue");
			return $msg;
		}
		
		$search = strtolower(trim($search)); // trim the whitepace		
		$bcond = ( !empty($table_name) && !empty($fld_name) && !empty($search) ); // check the predominant fields
		
		if(!$bcond){
			$msg = array("status" => "fail", "message" => "Table Name or Field name or Search text missing");
			return $msg;
		}
		
		$tcond = (isset($this->tables[$table_name])); // check the table is exist or not.
		if($tcond){
			$api_instance = new Swagger\Client\Api\SearchApi();
			$atoken = $this->get_fbuser_data("user_token");
			$tbl_id = $this->tables[$table_name];
			$type = "RECORD";
			$query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
			$queryObj = array();
			// Set the query string search
			$qmain = new stdClass();
			if(!empty($search))
			{
				$qmain->query_string = new stdClass();
				$qmain->query_string->query = $search;
			}
			
			$queryObj['query']= $qmain;
			$queryObj['size'] = 1000;
			$queryObj['from'] = 0;
			$qstr = json_encode($queryObj);
			$query['query']= $qstr;
			$query['method']="GET";
			$repositary = "";
			$mapping = "";
			try {
				$oresult = $api_instance->search($atoken, $type, $query, $tbl_id, $repositary, $mapping);
				$sresult = $oresult->getResult();
				$aresult = json_decode($sresult, true);
				$total_count = isset($aresult["hits"]["total"]) ? $aresult["hits"]["total"]: "0";
				$result_set = isset($aresult["hits"]["hits"]) ? $aresult["hits"]["hits"] : array();
				if( !empty($result_set) )
				{
					$is_find = false;
					
					foreach($result_set as $row){
						$ckey = $row["_id"];
						$src = $row["_source"];
						$fld_val = isset($src[$fld_name]) ? $src[$fld_name] : "";
						$fld_val = strtolower(trim($fld_val));
						
						if(empty($rkey))
						{
							// check duplicate for create new record
							if($fld_val == $search){
								$is_find = true;
							}
							
						} else {
							// check duplicate for update record
							if($fld_val == $search && $rkey!= $ckey){
								$is_find = true;
							}
						}
					}
					
					if(!$is_find){
						$msg = array("status" => "success", "message" => "No duplicate");
						return $msg;
					}else{
						$msg = array("status" => "fail", "message" => "Duplicate occurred");
						return $msg;
					}
					
				}else{
					$msg = array("status" => "success", "message" => "No duplicate");
					return $msg;
				}
			} catch(Exception $e){
				$smsg = $e->getMessage();
				log_message('error', "Exception when calling SearchApi->search: ".$smsg);
				$msg = array("status" => "fail", "message" => "Search functionality error");
				return $msg;
			}
		} else {
			$msg = array("status" => "fail", "message" => "Invalid Table name");
			return $msg;
		}
	}
	
	
	public function update_others($table_name = '',$orecord = array(),$nrecord = array(), $rkey = ''){
		$pcond = ( !empty($table_name) && !empty($orecord) && !empty($nrecord) && !empty($rkey) );
		if($pcond){
			$affect_flds = $this->CI->config->item('affect_flds', 'fb_boodskap');
			$fld_name = $affect_flds[$table_name];
			$oval = $orecord[$fld_name];
			$nval = $nrecord[$fld_name];
			if(strtolower($nval) == strtolower($oval)){
				log_message('info', "We don't need to update >> ".date("d m Y h:i:s a", now()) );
			}else{
				
				$cdata = array();
				$carr = array(
					"table_name" => $table_name, "oval" => $oval, "nval" => $nval, "fld_name" => $fld_name, "rkey" => $rkey
				);
				
				log_message('info', "Update details >> ".date("d m Y h:i:s a", now()).json_encode($cdata) );
				
				$affect_list = $this->CI->config->item('affect_list', 'fb_boodskap');
				$clist = $affect_list[$table_name];
				
				foreach($clist as $citm){
					$flag = true;
					$tarr = explode(":",$citm);
					$ctbl = reset($tarr);
					$cfld = end($tarr);
					$crst = $this->search_list($ctbl, $oval);
					if($crst["status"]=="success"){
						if(!empty($crst["result_set"])){
							foreach($crst["result_set"] as $crow){
								$crkey = $crow["_id"];
								$cidata[$cfld] = $nval;
								$cidata["updatedtime"] = now();
								$cmsg = $this->update_record($ctbl, $cidata, $crkey, true);
								if($cmsg["status"]=="fail"){
									$flag =  false;
								}
							}
						}
					}
				}
				
				log_message('info', "We should update rest of the tables >> ".date("d m Y h:i:s a", now()) );
			}
			
		}else{
			log_message('error', "Table name OR Old record OR New Record missing");
		}
	}
	
	public function search_list($table_name, $search, $size = 10000){
		$api_instance = new Swagger\Client\Api\SearchApi();
		$atoken = $this->get_fbuser_data("user_token");
		$tbl_id = $this->tables[$table_name];
		$type = "RECORD";
		$query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
		$queryObj = array();

		$qmain = new stdClass();
		if(!empty($search))
		{
			$qmain->query_string = new stdClass();
			$qmain->query_string->query = $search;
		}

		$queryObj['query']= $qmain;
		$queryObj['size'] = $size;
		$queryObj['from'] = 0;
		$queryObj['sort'] = array("updatedtime" => array("order" => "desc" ));
		$qstr = json_encode($queryObj);
		$query['query']= $qstr;
		$query['method']="GET";
		$repositary = "";
		$mapping = "";
		try {
			$oresult = $api_instance->search($atoken, $type, $query, $tbl_id, $repositary, $mapping);
			$sresult = $oresult->getResult();
			$aresult = json_decode($sresult, true);
			$total_count = isset($aresult["hits"]["total"]) ? $aresult["hits"]["total"]: "0";
			$result_set = isset($aresult["hits"]["hits"]) ? $aresult["hits"]["hits"] : array();
			$msg = array("status" => "success", "message" => "Search result got it",
			"result_set" => $result_set);
			return $msg;
		} catch (Exception $e) {
			$smsg = $e->getMessage();
			log_message('error', "Exception when calling SearchApi->search: ".$smsg);
			$msg = array("status" => "fail", "message" => "Search functionality error");
			return $msg;
		}
		
	}
	
	public function update_current_stock($pond_name = '', $species_type = '', $count = '', $ptype = '', $pid = '', $sid = '' ){
		$table_name = "current_stock";
		$stocked_date = $this->CI->input->post("stocked_date");
		
		$udata = array("pondname" => $pond_name, "species_type" => $species_type,
		"pond_id" => $pid, "species_id" => $sid,
		"count" => $count, "updatedtime" => now());
		
		$mcond = ( !empty($pond_name) && !empty($species_type) && !empty($count) && !empty($ptype) );
		if($mcond){
			$aresult = $this->search_list($table_name, $pond_name);
			if($aresult["status"] == "success"){
				$cresult = $aresult["result_set"];
				if(!empty($cresult)){
					$crow = $cresult[0];
					$csrc = $crow["_source"];
					$crkey = $crow["_id"];
					$ccnt = $csrc["count"];
					$ncnt = ($ptype=="add") ? ($count + $ccnt) : ( $ccnt - $count) ;
					$udata["count"] = $ncnt;
					$udata["stocked_date"] = $csrc["stocked_date"];
					$this->update_record($table_name, $udata, $crkey);
					$msg = array("status" => "success", "message" => "Successfully updated current stock");
					return $msg;
				} else {
					$udata["stocked_date"] = !empty($stocked_date) ? $stocked_date : now();
					$udata["createdtime"] = now();
					$this->create_record($table_name, $udata);
					$msg = array("status" => "success", "message" => "Successfully added current stock");
					return $msg;
				}
			}
		}
	}
	
	public function update_activities($table_name = '', $udata = array(), $action = '', $odata = array(), $rkey = '') {
		$method = $table_name.'_update_activity';
		if(method_exists($this, $method))
		{
			log_message("info", "called main $table_name update activity >> ");
			return call_user_func_array(array($this, $method), 
			  array($udata, $action, $odata, $rkey));
		} else {
			log_message("info", "$method this method not available.");
		}
		  
	}

	private function ponds_update_activity($udata = array(), $action = '', $odata = array(), $rkey = ''){
		$table_name = 'ponds';
		$ref = "";
		$aref = array();
		$new_pname = $udata["pondname"];
		$auser = $this->updated_user();
		$aref = array_merge($aref, $auser);
		
		if($action == "add")
		{
			$aref["msg"] = "Pond $new_pname added";
			$aref['source'] = $udata;
		}
		
		if($action=="update")
		{
			$aref["msg"] = "Pond $new_pname updated";
			$aref['source'] = $udata;
			$aref['osource'] = $odata;
		}
		
		if($action == "delete")
		{
			$aref["msg"] = "Pond $new_pname deleted";
			$aref['source'] = $udata;
		}
		
		$ref = serialize($aref);
		
		$activity_data = array(
		  "activity_module" => "ponds", "activity_fldname" => "pondname", "activity_fldval" => $udata["pondname"],
		  "activity_fldid" => $rkey, "activity_type" => "system", "activity_action" => $action,
		  "activity_state" => "1", "activity_date" => $udata["built_date"],
		  "activity_ref" => $ref, "createdtime" => now(), "updatedtime" => now()
		);
		
		$this->create_record($this->tbl_actvy, $activity_data, true);
		
	}
	
	private function pond_cleaning_update_activity($udata = array(), $action = '', $odata = array(), $rkey = ''){
		$table_name = 'pond_cleaning';
		$ref = "";
		$aref = array();
		$pname = $udata["pondname"];
		$auser = $this->updated_user();
		$aref = array_merge($aref, $auser);
		
		if($action == "add")
		{
			$aref["msg"] = "Pond $pname cleaned";
			$aref["source"] = $udata;
		}
		
		if($action=="update")
		{
			$aref["msg"] = "Edit: Pond $pname cleaned";
			$aref["source"] = $udata;
			$aref["osource"] = $odata;
		}
		
		if($action == "delete")
		{
			$aref["msg"] = "Delete: Pond $pname cleaned";
			$aref["source"] = $udata;
		}
		
		$ref = serialize($aref);
		
		$activity_data = array(
		  "activity_module" => $table_name, "activity_fldname" => "pondname", "activity_fldval" => $pname,
		  "activity_fldid" => $rkey, "activity_type" => "system", "activity_action" => $action,
		  "activity_state" => "1", "activity_date" => $udata["cleaned_date"],
		  "activity_ref" => $ref, "createdtime" => now(), "updatedtime" => now()
		);
		
		$this->create_record($this->tbl_actvy, $activity_data, true);
	}
	
	private function distribution_update_activity($udata = array(), $action = '', $odata = array(), $rkey = ''){
		$table_name = 'distribution';
		$ref = "";
		
		$cnt = $udata["count"];
		$dbn_date = $udata["createdtime"];
		
		$fpnd_name = $udata["from_pond"];
		$fpnd_id = $udata["from_pond_id"];
		
		$tpnd_name = $udata["to_pond"];
		$tpnd_id = $udata["to_pond_id"];
		
		$species_type = $udata["species_type"];
		
		$aref1 = $aref2 = $aref3 = array();
		
		if($action == "add"){
			$aref1 = array( "msg" => "$cnt $species_type transferred to $tpnd_name", "source" => $udata );
			$aref2 = array( "msg" => "$cnt $species_type added from $fpnd_name" , "source" => $udata);
			$auser = $this->updated_user();
			$aref1 = array_merge($aref1, $auser);
			$aref2 = array_merge($aref2, $auser);
			
			$ref1 = serialize($aref1);
			$ref2 = serialize($aref2);
			
			$activity_data1 = array(
			  "activity_module" => $table_name, "activity_fldname" => "from_pond", "activity_fldval" => $fpnd_name,
			  "activity_fldid" => $rkey, "activity_type" => "ponds", "activity_action" => $action,
			  "activity_state" => "1", "activity_date" => $dbn_date,
			  "activity_ref" => $ref1, "createdtime" => now(), "updatedtime" => now()
			);
		
			$this->create_record($this->tbl_actvy, $activity_data1, true);
		
			$activity_data2 = array(
			  "activity_module" => $table_name, "activity_fldname" => "to_pond", "activity_fldval" => $tpnd_name,
			  "activity_fldid" => $rkey, "activity_type" => "ponds", "activity_action" => $action,
			  "activity_state" => "1", "activity_date" => $dbn_date,
			  "activity_ref" => $ref2, "createdtime" => now(), "updatedtime" => now()
			);
		
			$this->create_record($this->tbl_actvy, $activity_data2, true);
			
		}
		
		if($action == "update"){
			$aref3 = array( "msg" => "Edit: $cnt $species_type transferred to Pond $tpnd_name from Pond $fpnd_name", 
				"source" => $udata, "osource" => $odata );
			$auser = $this->updated_user();
		    $aref3 = array_merge($aref3, $auser);

			$ref3 = serialize($aref3);

			$activity_data3 = array(
			  "activity_module" => $table_name, "activity_fldname" => "to_pond:from_pond", "activity_fldval" => $tpnd_name.':'.$fpnd_name,
			  "activity_fldid" => $rkey, "activity_type" => "ponds", "activity_action" => $action,
			  "activity_state" => "1", "activity_date" => $dbn_date,
			  "activity_ref" => $ref3, "createdtime" => now(), "updatedtime" => now()
			);

			$this->create_record($this->tbl_actvy, $activity_data3, true);
		}
		
		
		if($action == "delete"){
			$aref3 = array( "msg" => "Delete:  $cnt $species_type transferred to Pond $tpnd_name from Pond $fpnd_name", 
				"source" => $udata );

			$auser = $this->updated_user();
			$aref3 = array_merge($aref3, $auser);
		
			$ref3 = serialize($aref3);

			$activity_data3 = array(
			  "activity_module" => $table_name, "activity_fldname" => "to_pond:from_pond", "activity_fldval" => $tpnd_name.':'.$fpnd_name,
			  "activity_fldid" => $rkey, "activity_type" => "ponds", "activity_action" => $action,
			  "activity_state" => "1", "activity_date" => $dbn_date,
			  "activity_ref" => $ref3, "createdtime" => now(), "updatedtime" => now()
			);

			$this->create_record($this->tbl_actvy, $activity_data3, true);
		}
		
		
	}
	
	private function harvest_update_activity($udata = array(), $action = '', $odata = array(), $rkey = ''){
		$table_name = 'harvest';
		$ref = "";
		$aref = array();
		
		$cnt = $udata["count"];
		$pnd_name = $udata["pondname"];
		$pnd_id = $udata["pond_id"];
		$st = $udata["species_type"];
		$hdate = $udata["_month"]."/".$udata["_day"]."/".$udata["_year"];
		$htime = fb_convert_time($hdate);
		
		$auser = $this->updated_user();
		$aref = array_merge($aref, $auser);
		
		if($action=="add"){
			$aref["msg"] = "$cnt $st harvested from $pnd_name";
			$aref["source"] = $udata;
		}
		
		if($action=="update"){
			$aref["msg"] = "Edit : $cnt $st harvested from $pnd_name";
			$aref["source"] = $udata;
			$aref["osource"] = $odata;
		}
		
		if($action=="delete"){
			$aref["msg"] = "Delete : $cnt $st harvested from $pnd_name";
			$aref["source"] = $udata;
		}
		
		$ref = serialize($aref);
		
		$activity_data = array(
			  "activity_module" => $table_name, "activity_fldname" => "pondname", "activity_fldval" => $pnd_name,
			  "activity_fldid" => $rkey, "activity_type" => "ponds", "activity_action" => $action,
			  "activity_state" => "1", "activity_date" => $htime,
			  "activity_ref" => $ref, "createdtime" => now(), "updatedtime" => now()
			);
		$this->create_record($this->tbl_actvy, $activity_data, true);
	}
	
	private function mortality_update_activity($udata = array(), $action = '', $odata = array(), $rkey = ''){
		$table_name = 'mortality';
		$ref = "";
		$aref = array();
		
		$cnt = $udata["count"];
		$pnd_name = $udata["pondname"];
		$pnd_id = $udata["pond_id"];
		$st = $udata["species_type"];
		$mdate = $udata["_month"]."/".$udata["_day"]."/".$udata["_year"];
		$mtime = fb_convert_time($mdate);
		
		$auser = $this->updated_user();
		$aref = array_merge($aref, $auser);
		
		if($action=="add"){
			$aref["msg"] = "$cnt $st reported as dead in Pond $pnd_name";
			$aref["source"] = $udata;
		}
		
		if($action=="update"){
			$aref["msg"] = "Edit: $cnt $st reported as dead in Pond $pnd_name";
			$aref["source"] = $udata;
			$aref["osource"] = $odata;
		}
		
		if($action=="delete"){
			$aref["msg"] = "Delete : $cnt $st reported as dead in Pond $pnd_name";
			$aref["source"] = $udata;
		}
		
		$ref = serialize($aref);
		
		$activity_data = array(
			  "activity_module" => $table_name, "activity_fldname" => "pondname", "activity_fldval" => $pnd_name,
			  "activity_fldid" => $rkey, "activity_type" => "ponds", "activity_action" => $action,
			  "activity_state" => "1", "activity_date" => $mtime,
			  "activity_ref" => $ref, "createdtime" => now(), "updatedtime" => now()
			);
		$this->create_record($this->tbl_actvy, $activity_data, true);
		
	}
	
	private function feeds_update_activity($udata = array(), $action = '', $odata = array(), $rkey = ''){
		$table_name = 'feeds';
		$ref = "";
		$aref = array();
		$fname = $udata["feedname"];
		
		$auser = $this->updated_user();
		$aref = array_merge($aref, $auser);
		
		if($action=="add"){
			$aref["msg"] = "Feed $fname added";
			$aref["source"] = $udata;
		}
		
		if($action=="update"){
			$ofname = $odata["feedname"];
			$aref["msg"] = "Edit: Feedname $ofname changed to $ofname";
			$aref["source"] = $udata;
			$aref["osource"] = $odata;
		}
		
		if($action=="delete"){
			$aref["msg"] = "Delete: $fname deleted";
			$aref["source"] = $udata;
		}
		
		$ref = serialize($aref);
		
		$activity_data = array(
			  "activity_module" => $table_name, "activity_fldname" => "feedname", "activity_fldval" => $udata["feedname"],
			  "activity_fldid" => $rkey, "activity_type" => "feeds", "activity_action" => $action,
			  "activity_state" => "1", "activity_date" => $mtime,
			  "activity_ref" => $ref, "createdtime" => now(), "updatedtime" => now()
			);

		$this->create_record($this->tbl_actvy, $activity_data, true);
		
	}	
	
	public function update_feed_stock($stock_id = '', $weight = '',$feed_name=''){
		$table_name = "feed_stock";
		$udata = array("current_weight" => $weight,  "updatedtime" => now());
		
		$mcond = ( !empty($weight) );
		if($mcond){
			$aresult = $this->search_list($table_name, $feed_name);
			if($aresult["status"] == "success"){
				$cresult = $aresult["result_set"];
				if(!empty($cresult)){
					$crow = $cresult[0];
					$csrc = $crow["_source"];
					$crkey = $crow["_id"];
					$ccnt = $csrc["current_weight"];
					$ncnt =  $ccnt - $weight ;
					$udata["current_weight"] = $ncnt;
					$this->update_record($table_name, $udata, $crkey);
					$msg = array("status" => "success", "message" => "Successfully updated current stock");
					return $msg;
				} 
			}
		}
	}
	
	private function species_update_activity($udata = array(), $action = '', $odata = array(), $rkey = ''){
		$table_name = 'species';
		$ref = "";
		$aref = array();
		$stype = $udata["species_type"];
		
		$auser = $this->updated_user();
		$aref = array_merge($aref, $auser);
		
		if($action=="add"){
			$aref["msg"] = "Species $stype added";
			$aref["source"] = $udata;
		}
		
		if($action=="update"){
			$ostype = $odata["species_type"];
			$aref["msg"] = "Edit: Species $ostype updated as $stype";
			$aref["source"] = $udata;
			$aref["osource"] = $odata;
		}
		
		if($action=="delete"){
			$aref["msg"] = "Delete: Species $stype deleted";
			$aref["source"] = $udata;
		}
		
		$ref = serialize($aref);
		
		$activity_data = array(
			  "activity_module" => $table_name, "activity_fldname" => "species_type", "activity_fldval" => $udata["species_type"],
			  "activity_fldid" => $rkey, "activity_type" => "species", "activity_action" => $action,
			  "activity_state" => "1", "activity_date" => $udata["createdtime"],
			  "activity_ref" => $ref, "createdtime" => now(), "updatedtime" => now()
			);

		$this->create_record($this->tbl_actvy, $activity_data, true);
		
	}
	
	private function fish_stock_update_activity($udata = array(), $action = '', $odata = array(), $rkey = ''){
		$table_name = 'fish_stock';
		$ref = "";
		
		$fsdate = $udata["_month"]."/".$udata["_day"]."/".$udata["_year"];
		$fstime = fb_convert_time($fsdate);
		$pname = $udata["pondname"];
		$cnt = $udata["count"];
		$sp = $udata["species_type"];
		
		$aref = array();
		
		$auser = $this->updated_user();
		$aref = array_merge($aref, $auser);
		
		if($action == "add"){
			$aref["msg"] = "$cnt $sp stocked in Pond $pname";
			$aref["source"] = $udata;
		}
		
		if($action == "update"){
			$aref["msg"] = "Edit: $cnt $sp stocked in Pond $pname";
			$aref["source"] = $udata;
			$aref["osource"] = $odata;
		}
		
		if($action == "delete"){
			$aref["msg"] = "Delete: $cnt $sp stocked in Pond $pname";
			$aref["source"] = $udata;
		}
		
		$ref = serialize($aref);
		
		$activity_data = array(
			  "activity_module" => $table_name, "activity_fldname" => "pondname", "activity_fldval" => $pname,
			  "activity_fldid" => $rkey, "activity_type" => "ponds", "activity_action" => $action,
			  "activity_state" => "1", "activity_date" => $fstime,
			  "activity_ref" => $ref, "createdtime" => now(), "updatedtime" => now()
			);

		$this->create_record($this->tbl_actvy, $activity_data, true);
		
	}
	
	private function current_stock_update_activity($udata = array(), $action = '', $odata = array(), $rkey = ''){
		$table_name = 'current_stock';
		$ref = "";
		
		$cstime = $udata["createdtime"];
		$pname = $udata["pondname"];
		$cnt = $udata["count"];
		$sp = $udata["species_type"];
		
		$aref = array();
		
		$auser = $this->updated_user();
		$aref = array_merge($aref, $auser);
		
		if($action == "add"){
			$aref["msg"] = "Current stock: Pondname : $pname, Count : $cnt, Species: $sp";
			$aref["source"] = $udata;
		}
		
		if($action == "update"){
			$aref["msg"] = "Current stock Edit:Pondname : $pname, Count : $cnt, Species: $sp";
			$aref["source"] = $udata;
			$aref["osource"] = $odata;
		}
		
		if($action == "delete"){
			$aref["msg"] = "Current stock has been deleted successfully. Pondname : $pname, Count : $cnt, Species: $sp";
			$aref["source"] = $udata;
		}
		
		$ref = serialize($aref);
		
		$activity_data = array(
			  "activity_module" => $table_name, "activity_fldname" => "pondname", "activity_fldval" => $pname,
			  "activity_fldid" => $rkey, "activity_type" => "ponds", "activity_action" => $action,
			  "activity_state" => "1", "activity_date" => $cstime,
			  "activity_ref" => $ref, "createdtime" => now(), "updatedtime" => now()
			);

		$this->create_record($this->tbl_actvy, $activity_data, true);
		
	}
	
	private function sampling_update_activity($udata = array(), $action = '', $odata = array(), $rkey = ''){
		$table_name = 'sampling';
		$ref = "";
		$pname = $udata["pondname"];
		$sdate = $udata["sampling_date"];
		
		$aref = array();
		
		$auser = $this->updated_user();
		$aref = array_merge($aref, $auser);
		
		if($action == "add"){
			$aref["msg"] = "Sampling done for Pond $pname.";
			$aref["source"] = $udata;
		}
		
		if($action == "update"){
			$aref["msg"] = "Edit: Sampling done for Pond $pname";
			$aref["source"] = $udata;
			$aref["osource"] = $odata;
		}
		
		if($action == "delete"){
			$aref["msg"] = "Delete: Sampling done for Pond $pname";
			$aref["source"] = $udata;
		}
		
		$ref = serialize($aref);
		
		$activity_data = array(
			  "activity_module" => $table_name, "activity_fldname" => "pondname", "activity_fldval" => $pname,
			  "activity_fldid" => $rkey, "activity_type" => "ponds", "activity_action" => $action,
			  "activity_state" => "1", "activity_date" => $sdate,
			  "activity_ref" => $ref, "createdtime" => now(), "updatedtime" => now()
			);

		$this->create_record($this->tbl_actvy, $activity_data, true);
	}
	
	private function feed_stock_update_activity($udata = array(), $action = '', $odata = array(), $rkey = ''){
		$table_name = 'feed_stock';
		$ref = "";
		$aref = array();
		$feed_name = $udata["feed_name"];
		$stocked_weight = $udata["stocked_weight"];
		$fsdate = $udata["_month"]."/".$udata["_day"]."/".$udata["_year"];
		$fstime = fb_convert_time($fsdate);
		
		$auser = $this->updated_user();
		$aref = array_merge($aref, $auser);
		
		if($action == "add"){
			$aref["msg"] = "$stocked_weight $feed_name feed stocked";
			$aref["source"] = $udata;
		}
		
		if($action == "update"){
			$aref["msg"] = "Edit: $stocked_weight $feed_name feed stocked";
			$aref["source"] = $udata;
			$aref["osource"] = $odata;
		}
		
		if($action == "delete"){
			$aref["msg"] = "Delete: $stocked_weight $feed_name feed";
			$aref["source"] = $udata;
		}
		
		$ref = serialize($aref);
		
		$activity_data = array(
			  "activity_module" => $table_name, "activity_fldname" => "feed_name", "activity_fldval" => $feed_name,
			  "activity_fldid" => $rkey, "activity_type" => "feeds", "activity_action" => $action,
			  "activity_state" => "1", "activity_date" => $fstime,
			  "activity_ref" => $ref, "createdtime" => now(), "updatedtime" => now()
			);

		$this->create_record($this->tbl_actvy, $activity_data, true);
		
	}
	
	private function feed_distribution_update_activity($udata = array(), $action = '', $odata = array(), $rkey = ''){
		$table_name = 'feed_distribution';
		$ref = "";
		$aref = array();
		
		$pname = $udata["pond_name"];
		$fd_date = $udata["date"];
		
		$stock_id = $udata["feed_stock_id"];
		$pond_id = $udata["pond_id"];
		
		$stock = $this->fb_rest->get_record("feed_stock", $stock_id);
		$feed_name = $stock['result_set']['feed_name'];
		
		$pond = $this->fb_rest->get_record("ponds", $pond_id);
		$pname = $pond['result_set']['pondname'];
		
		$auser = $this->updated_user();
		$aref = array_merge($aref, $auser);
		
		if($action == "add"){
			$aref["msg"] = "Feed $feed_name distributed to Pond $pname";
			$aref["source"] = $udata;
		}
		
		if($action == "update"){
			$aref["msg"] = "Edit: Feed $feed_name distributed to Pond $pname";
			$aref["source"] = $udata;
			$aref["osource"] = $odata;
		}
		
		if($action == "delete"){
			$aref["msg"] = "Delete: Feed $feed_name distributed to Pond $pname";
			$aref["source"] = $udata;
		}
		
		$ref = serialize($aref);
		
		$activity_data = array(
			  "activity_module" => $table_name, "activity_fldname" => "pondname", "activity_fldval" => $pname,
			  "activity_fldid" => $rkey, "activity_type" => "ponds", "activity_action" => $action,
			  "activity_state" => "1", "activity_date" => $fd_date,
			  "activity_ref" => $ref, "createdtime" => now(), "updatedtime" => now()
			);

		$this->create_record($this->tbl_actvy, $activity_data, true);
	}
	
	private function updated_user(){
		$email = $this->get_fbuser_data("email");
		$first_name = $this->get_fbuser_data("first_name");
		$last_name = $this->get_fbuser_data("last_name");
		$auser = compact("email", "first_name", "last_name");
		return $auser;
	}
	
	public function lod_more($table_name,$from,$per_page){
		if(!$this->isloggedin()){
			$msg = array("status" => "fail", "message" => "Please login to continue");
			return $msg;
		}
		
		
		$tcond = isset($this->tables[$table_name]);
		if($tcond){
			$api_instance = new Swagger\Client\Api\SearchApi();
			$atoken = $this->get_fbuser_data("user_token");
			$tbl_id = $this->tables[$table_name];
			$type = "RECORD";
			$query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
			$queryObj = array();
			
			$qmain = new stdClass();

			$search = "*";
			$qmain->query_string = new stdClass();
			$qmain->query_string->query = $search;
			
			
			$queryObj['query']= $qmain;
			$queryObj['size'] = $per_page;
			$queryObj['from'] = $from;
			$queryObj['sort'] = array("updatedtime" => array("order" => "desc" ));
			$qstr = json_encode($queryObj);
			$query['query']= $qstr;
			$query['method']="GET";
			$repositary = "";
			$mapping = "";
			try {
				$oresult = $api_instance->search($atoken, $type, $query, $tbl_id, $repositary, $mapping);
				$sresult = $oresult->getResult();
				$aresult = json_decode($sresult, true);
				$result_set = isset($aresult["hits"]["hits"]) ? $aresult["hits"]["hits"] : array();
				$msg = array("status" => "success", "message" => "Search result got it",
			     "result_set" => $result_set);
				return $msg;
			} catch (Exception $e) {
				$smsg = $e->getMessage();
				log_message('error', "Exception when calling SearchApi->search: ".$smsg);
				$msg = array("status" => "fail", "message" => "Search functionality error");
				return $msg;
			}
			
		}else{
			$msg = array("status" => "fail", "message" => "Invalid Table name");
			return $msg;
		}
		
		
	}
	
}
