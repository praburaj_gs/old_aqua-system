<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['dashboard'] = "डैशबोर्ड";
$lang['menus'] = "मेनू";

// Dashboard
$lang['ponds'] = "तालाबों";
$lang['pond_list'] = "तालाब सूची";
$lang['pond_cleaning'] = "तालाब सफाई";
$lang['species'] = "प्रजाति ";
$lang['feed'] = "चारा ";
$lang['feed_list'] = "चारा सूची";
$lang['feed_stock_list'] = "फीड भण्डार लिस्ट ";
$lang['feed_distribution'] = "फीड वितरण";
$lang['stock'] = "स्टॉक ";
$lang['current_stock'] = "करंट स्टॉक ";
$lang['fish_stock'] = "फिश स्टॉक ";
$lang['distribution'] = "डिस्ट्रीब्यूशन ";
$lang['harvest'] = "हार्वेस्ट ";
$lang['mortality'] = "मोर्टेलिटी ";
$lang['action'] = "कार्य";

//pond list
$lang['pond_name'] = "पोंड नाम ";
$lang['length'] = "लंबाई";
$lang['width'] = "चौड़ाई";
$lang['depth'] = "गहराई";
$lang['built_date'] = "बिल्ट डेट";
$lang['add_pond'] = "ऐड पोंड";
$lang['width_in_feet'] = "चौड़ाई इन पैर का पंजा";
$lang['depth_in_feet'] = "डेप्थ इन पैर का पंजा";
$lang['length_in_feet'] = "लंबाई इन पैर का पंजा";
$lang['edit_pond'] = "तालाब संपादित करें";

//pond cleaning
$lang['cleaned_by'] = "क्लीनेड बी ";
$lang['cleaned_date'] = "क्लीनेड डेट";
$lang['date_of_clenaing'] = "डेट ऑफ़ क्लीन";
$lang['add_pond_cleaning'] = "ऐड पोंड क्लीनिंग";
$lang['edit_pond_cleaning'] = "एडिट पोंड क्लीनिंग ";

//species
$lang['species_name'] = "स्पीशीज नाम";
$lang['feed_name'] = "फीड नाम";
$lang['market_size'] = "मार्किट साइज";
$lang['add_species'] = "स्पीशीज ऐड";
$lang['edit_species'] = "स्पीशीज एडिट";

//feed list
$lang['feed_type'] = "फीड टाइप";
$lang['size'] = "साइज";
$lang['add_feed'] = "ऐड फीड";
$lang['edit_feed'] = "ऐड फीड";

//feed stock list
$lang['stocked_weight'] = "स्टॉकेड वेट";
$lang['current_weight'] = "करंट वेट";
$lang['date'] = "डेट";
$lang['weight_in_kg'] = "वेट इन कग";
$lang['add_feed_stock'] = "ऐड फीड स्टॉक";
$lang['edit_feed_stock'] = "एडिट फीड स्टॉक";

//feed distribution
$lang['feed_stock_distribution_list'] = "फीड स्टॉक डिस्ट्रीब्यूशन लिस्ट";
$lang['add_feed_stock_distribution'] = "ऐड फीड स्टॉक डिस्ट्रीब्यूशन";
$lang['edit_feed_stock_distribution'] = "एडिट फीड स्टॉक डिस्ट्रीब्यूशन";
$lang['weight'] = "वेट";

//current stock
$lang['count'] = "काउंट";
$lang['species_type'] = "स्पीशीज टाइप";
$lang['modified'] = "मॉडिफाइड";

//fish stock
$lang['fertilizer'] = "फ़र्टिलाइज़र";
$lang['water_type'] = "वाटर टाइप";
$lang['weight_in_grams'] = "ग्राम में वजन";
$lang['add_fish_stock'] = "मछली का स्टॉक जोड़ें";
$lang['edit_fish_stock'] = "मछली स्टॉक को संपादित करें";

//distribution
$lang['from_pond'] = "तालाब से";
$lang['to_pond'] = "तालाब में";
$lang['updated_by'] = "द्वारा अपडेट";
$lang['add_distribution'] = "ऐड डिस्ट्रीब्यूशन";

//harvest
$lang['add_harvest'] = "ऐड हार्वेस्ट";

//morality
$lang['add_mortality'] = "जोड़ना मृत्यु-दर";
$lang['reason'] = "कारण";

//sampling
$lang['avg_weight'] = "औसत वजन";
$lang['fish_length'] = "मछली की लंबाई";
$lang['fish_count'] = "मछली गणना";
$lang['observation'] = "अवलोकन";

// Common
$lang["show"] = "प्रदर्शन";
$lang["entries"] = "प्रविष्टियां";
$lang["search"] = "खोज";
$lang["success"] = "Grabación creada";
$lang["failed"] = "Error de inserción, ¡inténtelo de nuevo!";
$lang["update_success"] = "Registro actualizado";
$lang["update_failed"] = "La actualización falló, ¡Inténtalo de nuevo!";
$lang["delete_success"] = "Registro eliminado";
$lang["delete_failed"] = "Falló, por favor intente nuevamente!";
$lang["choose"] = "चुनें...";