<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['dashboard'] = "tableau de bord";
$lang['menus'] = "Menus";

// Dashboard
$lang['ponds']="étangs";
$lang['pond_list'] = "étangs liste";
$lang['pond_cleaning'] = "étangs Nettoyage";
$lang['species'] = "Espèce";
$lang['feed'] = "Alimentation";
$lang['feed_list'] = "Alimentation liste";
$lang['feed_stock_list'] = "Alimentation Stock liste";
$lang['feed_distribution'] = "Alimentation Distribution";
$lang['stock'] = "Stock";
$lang['current_stock'] = "Current Stock";
$lang['fish_stock'] = "Poisson Stock";
$lang['distribution'] = "Distribution";
$lang['harvest'] = "Récolte";
$lang['mortality'] = "mortalité";
$lang['action']="Action";

//pond list
$lang['pond_name']="Nom du bassin";
$lang['length']="Longueur";
$lang['width']="Largeur";
$lang['depth']="Profondeur";
$lang['built_date']="Date de fabrication";
$lang['add_pond']="Ajouter un étang";
$lang['width_in_feet']="Largeur en pieds";
$lang['depth_in_feet']="profondeur en pieds";
$lang['length_in_feet']="Longueur en pieds";
$lang['edit_pond']="Modifier l'étang";

//pond cleaning
$lang['cleaned_by']="Nettoyé par";
$lang['cleaned_date']="Date nettoyée";
$lang['date_of_clenaing']="Date de nettoyage";
$lang['add_pond_cleaning']="Ajouter un nettoyage de bassin";
$lang['edit_pond_cleaning']="Modifier le nettoyage du bassin ";

//species
$lang['species_name'] = "Nom de l'espèce";
$lang['feed_name'] = "Nom du flux";
$lang['market_size'] = "Market SizeLa taille du marché
";
$lang['add_species'] = "Ajouter des espèces";
$lang['edit_species'] = "modifier Espèces";

//feed list
$lang['feed_type'] = "Type d'alimentation";
$lang['size'] = "Taille";
$lang['add_feed'] = "Ajouter un flux";
$lang['edit_feed']="Modifier le flux";

//feed stock list
$lang['stocked_weight'] = "Poids stocké";
$lang['current_weight'] = "Poids actuel";
$lang['date'] = "Rendez-vous amoureux";
$lang['weight_in_kg'] = "Poids en kg";
$lang['add_feed_stock'] = "Ajouter un stock d'alimentation";
$lang['edit_feed_stock'] = "Modifier le stock d'alimentation";

//feed distribution
$lang['feed_stock_distribution_list'] = "alimentation Stock Liste de distribution";
$lang['add_feed_stock_distribution'] = "Ajouter une distribution de stock de flux";
$lang['edit_feed_stock_distribution'] = "Modifier la distribution du stock de flux";
$lang['weight'] = "Poids";

//current stock
$lang['count'] = "compter";
$lang['species_type'] = "Type d'espèce";
$lang['modified'] = "Modifié";

//fish stock
$lang['fertilizer'] = "Engrais";
$lang['water_type'] = "Type d'eau";
$lang['weight_in_grams'] = "Poids en grammes";
$lang['add_fish_stock'] = "Ajouter du poisson";
$lang['edit_fish_stock'] = "Modifier le stock de poisson";

//distribution
$lang['from_pond'] = "De l'étang";
$lang['to_pond'] = "Pour étang";
$lang['updated_by'] = "Mis à jour par";
$lang['add_distribution'] = "Ajouter une distribution";

//harvest
$lang['add_harvest'] = "Ajouter une récolte";

//morality
$lang['add_mortality'] = "Ajouter la mortalité";
$lang['reason'] = "Raison";

//sampling
$lang['avg_weight'] = "Poids moyen";
$lang['fish_length'] = "Longueur de poisson";
$lang['fish_count'] = "Nombre de poissons";
$lang['observation'] = "Observation";

// Common
$lang["show"] = "Montrer";
$lang["entries"] = "Entrées";
$lang["search"] = "Chercher";
$lang["success"] = "Enregistrement créé";
$lang["failed"] = "L'insertion a échoué, essayez encore!";
$lang["update_success"] = "Enregistrement mis à jour";
$lang["update_failed"] = "La mise à jour a échoué, essayez encore!";
$lang["delete_success"] = "Enregistrement supprimé";
$lang["delete_failed"] = "Échec, veuillez réessayer!";
$lang["choose"] = "choisir...";