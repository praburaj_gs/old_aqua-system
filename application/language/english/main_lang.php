<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['dashboard'] = "Dashboard";
$lang['menus'] = "Menus";

// Dashboard
$lang['ponds']="Ponds";
$lang['pond_list'] = "Pond List";
$lang['pond_cleaning'] = "Pond Cleaning";
$lang['species'] = "Species";
$lang['feed'] = "Feed";
$lang['feed_list'] = "Feed List";
$lang['feed_stock_list'] = "Feed Stock List";
$lang['feed_distribution'] = "Feed Distribution";
$lang['stock'] = "Stock";
$lang['current_stock'] = "Current Stock";
$lang['fish_stock'] = "Fish Stock";
$lang['distribution'] = "Distribution";
$lang['harvest'] = "Harvest";
$lang['mortality'] = "Mortality";
$lang['action']="Action";

//pond list
$lang['pond_name']="Pond Name";
$lang['length']="Length";
$lang['width']="Width";
$lang['depth']="Depth";
$lang['built_date']="Built Date";
$lang['add_pond']="Add Pond";
$lang['width_in_feet']="Width in Feet";
$lang['depth_in_feet']="Depth in Feet";
$lang['length_in_feet']="Length in Feet";
$lang['edit_pond']="Edit Pond";

//pond cleaning
$lang['cleaned_by']="Cleaned By";
$lang['cleaned_date']="Cleaned Date";
$lang['date_of_clenaing']="Date Of Cleaning";
$lang['add_pond_cleaning']="Add Pond Cleaning";
$lang['edit_pond_cleaning']="Edit Pond Cleaning ";

//species
$lang['species_name'] = "Species Name";
$lang['feed_name'] = "Feed Name";
$lang['market_size'] = "Market Size";
$lang['add_species'] = "Add Species";
$lang['edit_species'] = "Edit Species";

//feed list
$lang['feed_type'] = "Feed Type";
$lang['size'] = "Size";
$lang['add_feed'] = "Add Feed";
$lang['edit_feed']="Edit Feed";

//feed stock list
$lang['stocked_weight'] = "Stocked Weight";
$lang['current_weight'] = "Current Weight";
$lang['date'] = "Date";
$lang['weight_in_kg'] = "Weight in KG";
$lang['add_feed_stock'] = "Add Feed Stock";
$lang['edit_feed_stock'] = "Edit Feed Stock";

//feed distribution
$lang['feed_stock_distribution_list'] = "Feed Stock Distribution List";
$lang['add_feed_stock_distribution'] = "Add Feed Stock Distribution";
$lang['edit_feed_stock_distribution'] = "Edit Feed Stock Distribution";
$lang['weight'] = "Weight";

//current stock
$lang['count'] = "count";
$lang['species_type'] = "Species Type";
$lang['modified'] = "Modified";

//fish stock
$lang['fertilizer'] = "Fertilizer";
$lang['water_type'] = "Water Type";
$lang['weight_in_grams'] = "Weight in Grams";
$lang['add_fish_stock'] = "Add Fish Stock";
$lang['edit_fish_stock'] = "Edit Fish Stock";

//distribution
$lang['from_pond'] = "From Pond";
$lang['to_pond'] = "To Pond";
$lang['updated_by'] = "Updated By";
$lang['add_distribution'] = "Add Distribution";

//harvest
$lang['add_harvest'] = "Add Harvest";

//morality
$lang['add_mortality'] = "Add Mortality";
$lang['reason'] = "Reason";

//sampling
$lang['avg_weight'] = "Average Weight";
$lang['fish_length'] = "Fish Length";
$lang['fish_count'] = "Fish Count";
$lang['observation'] = "Observation";

// Common
$lang["show"] = "Show";
$lang["entries"] = "Entries";
$lang["search"] = "Search";
$lang["success"] = "Record Created";
$lang["failed"] = "Insert failed, Try Again!";
$lang["update_success"] = "Record Updated";
$lang["update_failed"] = "Update failed, Try Again!";
$lang["delete_success"] = "Record Deleted";
$lang["delete_failed"] = "Failed, Please Try Again!";
$lang["choose"] = "Choose...";