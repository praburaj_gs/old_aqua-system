<?php

class MY_Controller extends CI_Controller
{
	public function __construct() {
		parent::__construct();
		$alang = fb_cur_lang();
		$this->lang->load('main', $alang["lang_name"]);
	}
}