jQuery(document).ready(function($) { 
  // place your code here
  $("#species-form").validate({
	  	onkeyup: false,
	   	onclick: false,
	   	onfocusout: false,
		rules: {
			feedname: {
				required: true,
			},
			speciestype:{
				required: true,
				chkduplicate: '{ "table_name": "species", "fld_name": "species_type"}'
			},
			maxweight:{
				required: true,
				number: true,
				min :0
			}
		},
		messages: {
			feedname: {
				required:  disp_text("err_fname"),
			},
			speciestype: {
				required:  disp_text("err_stype"),
				chkduplicate:  disp_text("err_stype_exist")
			},
			maxweight: {
				required:  disp_text("err_maxweight"),
				number : disp_text("err_num"),
				min : disp_text("err_gt0")
			}
		}
	});
	
  $("#feed-form").validate({
	  	onkeyup: false,
	   	onclick: false,
	   	onfocusout: false,
		rules: {
			feedname: {
				required: true,
				chkduplicate: '{ "table_name": "feeds", "fld_name": "feedname"}',
			},
			feed_type:{
				required: true
			},
			size:{
				required: true,
				number: true,
				min:0
			}
		},
		messages: {
			
			feedname: {
				required: disp_text("err_fname"),
				chkduplicate: disp_text("err_fname_exists")
			},
			feed_type: {
				required: disp_text("err_ftype")
			},
			size: {
				required: disp_text("err_fsize"),
				number : disp_text("err_num"),
				min : disp_text("err_gt0")
			}
		}
	});
	
	//view feed

	$('.view-modal').on('click', function(event) {
    var $row = $(this).closest("tr");    // Find the row
    var $tds = $row.find("td");
    $.each($tds, function(index,value) {
		switch(index) {
			case 0:
				$("#feednamemodel").html($(this).text());
			break;
			case 1:
				$("#feedtypemodel").html($(this).text());
			break;
			case 2:
				$("#feedsizemodel").html($(this).text());
			break;
			
		} 
    });
    

	});
	//view currentstock
	$('.view-modal').on('click', function(event) {
    var $row = $(this).closest("tr");    // Find the row
    var $tds = $row.find("td");
    $.each($tds, function(index,value) {
     		
		switch(index) {
			case 0:
				$("#pondnamemodel").html($(this).text());
			break;
			case 1:
				$("#countcsmodel").html($(this).text());
			break;
			case 2:
				$("#speciestypemodel").html($(this).text());
			break;
			case 3:
				$("#modifiedmodel").html($(this).text());
			break;
		} 
    });
	});
	//view species

	$('.view-modal').on('click', function(event) {
    var $row = $(this).closest("tr");    // Find the row
    var $tds = $row.find("td");
    $.each($tds, function(index,value) {
		switch(index) {
			case 0:
				$("#speciesnamemodel").html($(this).text());
			break;
			case 1:
				$("#speciesfeedmodel").html($(this).text());
			break;
			case 2:
				$("#speciesmaxweightmodel").html($(this).text());
			break;
			
		} 
    });
    

	});
	var baseUrl = $('#base').val();
	//edit ajax call
	$('.edit-feed').on('click', function(){
		var rid = $(this).data('id');
		var card = $('#feedform-card .card-body');
		card.before('<div class="ajax-spinner" style="display:none;"><img src="'+baseUrl+'/assets/images/fish-loader.gif" /></div>');
		var height=card.height();
		var width=card.width();
		card.css('opacity','0.1');		
		$('.ajax-spinner').show();
		$.ajax({
			type:"POST",
			url:"feed/editFeed",
			data: { 'rid' : rid },
		  }).done(function(data) {
			 $('#feedform-card').empty();
			 $('#feedform-card').html(data);
			 $('.ajax-spinner').hide();
			 $('#feedform-card .card-body').css('opacity','1');
		  });
	});
	
	$('.edit-species').on('click', function(){
		var rid = $(this).data('id');
		var card = $('#speciesform-card .card-body');
		card.before('<div class="ajax-spinner" style="display:none;"><img src="'+baseUrl+'/assets/images/fish-loader.gif" /></div>');
		var height=card.height();
		var width=card.width();
		card.css('opacity','0.1');		
		$('.ajax-spinner').show();
		$.ajax({
			type:"POST",
			url:"species/editSpecies",
			data: { 'rid' : rid },
		  }).done(function(data) {
			 $('#speciesform-card').empty();
			 $('#speciesform-card').html(data);
			 $('.ajax-spinner').hide();
			 $('#speciesform-card .card-body').css('opacity','1');
		  });
	});
	
	$("#samplinglist-form").validate({
	  	onkeyup: false,
	   	onclick: false,
	   	onfocusout: false,
		rules: {
			pondname: {
				required: true,
				
			},
			species_type:{
				required: true
			},
			count:{
				required: true,
				number: true,
				min:0
			},
			fish_length:{
				required: true,
				number: true,
				min:0
			},
			avg_weight:{
				required: true,
				number: true,
				min:0
			},
			sampling_date:{
				required: true,
			}
			
		},
		messages: {
			
			count: {
				required: disp_text("err_count"),
				number : disp_text("err_num"),
				min : disp_text("err_gt0")
			},
			fish_length: {
				required: disp_text("err_flength"),
				number : disp_text("err_num"),
				min : disp_text("err_gt0")
			},
			avg_weight: {
				required: disp_text("err_avgweight"),
				number : disp_text("err_num"),
				min : disp_text("err_gt0")
			},
			sampling_date: {
				required: disp_text("err_date"),
				number : disp_text("err_num"),
				min : disp_text("err_gt0")
			}
			
		}
	});
	
	$('.sampling').on('click', function(event) {
		$('#samplinglist-form')[0].reset();
		var chk_url = site_url + "sampling/search_list";
		var cobj = $(this);
		var pname = cobj.attr("data-pname");
		var stype = cobj.attr("data-stype");
		$("#spondnamemodel").val(pname);
		$("#sspeciestypemodel").val(stype);
		$("#pondnameLabel").html("Sampling - ( "+pname+"-"+stype+")");
		var card = $('#samplinglist-form .modal-content');
		card.before('<div class="ajax-spinner" style="display:none;"><img src="'+baseUrl+'/assets/images/fish-loader.gif" /></div>');
		card.css('opacity','0.5');
		$('.ajax-spinner').show();
		
		$.ajax({
			type: "POST",
			data: { 'pondname' : pname },
			url: chk_url,
			}).done(function(result){
			$('.ajax-spinner').hide();
			card.css('opacity','1');
			var res = JSON.parse(result);
			if(res.status == "success"){
				var rst = res.result_set;
				if(rst.length==0){
					$("#countmodel").val("");
					$("#fishlengthmodel").val("");
					$("#avgweightmodel").val("");
					$("#samplingdatemodel").val("");
					$("#observationmodel").val("");
					$('#samplinglist-form .modal-content').show();
				}
				else{				
				var src = rst[0]._source || '';
				if(src.pondname == pname)
				{
					$('#samplinglist-form .modal-content').show();
					var count = src.count;
					var fish_length = src.fish_length;
					var avg_weight = src.avg_weight;
					var sampling_date=src.sampling_date;
					var sd = sampling_date;
					var t = sd + "000";
					t = parseInt(t);
					var dobj = new Date(t);
					var sdate =  (dobj.getMonth() + 1) + "/" + dobj.getDate() + "/" + dobj.getFullYear();
					var observation=src.observation;
					$("#countmodel").val(count);
					$("#fishlengthmodel").val(fish_length);
					$("#avgweightmodel").val(avg_weight);
					$("#samplingdatemodel").val(sdate);
					$("#observationmodel").val(observation);
				}
				}
			}
				
				
		}).fail(function(err){
			console.log(err);
		});
			
	});
});