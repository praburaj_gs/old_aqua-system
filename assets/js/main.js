$(document).ready(function($) {

    "use strict";

    [].slice.call(document.querySelectorAll('select.cs-select')).forEach(function(el) {
        new SelectFx(el);
    });

    jQuery('.selectpicker').selectpicker;

    jQuery.validator.addMethod("validate_email", function(value, element) {
        if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
            return true;
        } else {
            return false;
        }
    }, "Please enter a valid Email.");

    jQuery.validator.addMethod("notEqual", function(value, element, param) {
        return this.optional(element) || value != $(param).val();
    }, "This has to be different pond.");


    $.validator.addMethod("chkduplicate", function(value, element, arg) {
        console.log(value);
        var pms = JSON.parse(arg);
        pms["search"] = value;
        var rkey = $(element).closest("form").find("input[name=rkey]").val() || "";
        pms["rkey"] = rkey;
        var chk_url = site_url + "common/check_duplicate";
        var flg = false;
        $.ajax({
            type: "GET",
            data: pms,
            url: chk_url,
            async: false,
        }).done(function(resp) {
            var rdata = JSON.parse(resp);
            console.log(resp);
            console.log(rdata.status);
            if (rdata.status == "success") {
                console.log("success");
                flg = true;
            } else {
                console.log("fail");
                flg = false;
            }

        }).fail(function(err) {
            console.log(err);
        });
        console.log("123>>>>>>");
        //console.log(pms);
        return flg;
    }, "This field value already exists.");

    var baseUrl = $('#base').val();
    $('#menuToggle').on('click', function(event) {
        $('body').toggleClass('open');
    });

    $('.search-trigger').on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();
        $('.search-trigger').parent('.header-left').addClass('open');
    });

    $('.search-close').on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();
        $('.search-trigger').parent('.header-left').removeClass('open');
    });

    $("#login-form").validate({
        rules: {
            username: {
                required: true,
                validate_email: true
            },
            password: {
                required: true,
            },

        },
        messages: {

            username: {
                required: "Please enter a username",
                email: "Please enter a valid email address",
            },
            password: {
                required: "Please provide a password",
            },
        }
    });

    $("#stock-form").validate({
        rules: {
            pondname: {
                required: true,
            },
            count: {
                required: true,
                number: true,
                min: 0
            },
            species_type: {
                required: true,
            },
            weight: {
                required: true,
                number: true,
                min: 0
            },
            fertilizer: {
                required: true,
            },
            water_type: {
                required: true,
            },
            date: {
                required: true
            }
        },
        messages: {

            pondname: {
                required: disp_text("err_pname"),
            },
            count: {
                required: disp_text("err_count"),
                number: disp_text("err_num"),
                min: disp_text("err_gt0")
            },
            species_type: {
                required: disp_text("err_stype"),
            },
            weight: {
                required: disp_text("err_weight"),
                number: disp_text("err_num"),
                min: disp_text("err_gt0")
            },
            fertilizer: {
                required: disp_text("err_fertilizer"),
            },
            water_type: {
                required: disp_text("err_water_type"),
            },
            date: {
                required: disp_text("err_date"),
            }
        },
        errorPlacement: function(error, element) {

            if (element.attr("name") == "date")
                error.insertAfter(".datetimepicker-icon");
            else
                error.insertAfter(element);
        }

    });

    $("#distribution-form").validate({
        rules: {
            from_pond: {
                required: true,
            },
            to_pond: {
                required: true,
                notEqual: "#from_pond"
            },
            species_type: {
                required: true,
            },
            count: {
                required: true,
                number: true,
                min: 0
            },
            updatedby: {
                required: true,
            },
            date: {
                required: true
            }
        },
        messages: {

            from_pond: {
                required: disp_text("err_fpond"),
            },
            to_pond: {
                required: disp_text("err_tpond"),
            },
            species_type: {
                required: disp_text("err_stype"),
            },
            count: {
                required: disp_text("err_count"),
                number: disp_text("err_num"),
                min: disp_text("err_gt0")
            },
            updatedby: {
                required: disp_text("err_updatedby"),
            },
            date: {
                required: disp_text("err_date"),
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "date")
                error.insertAfter(".datetimepicker-icon");
            else
                error.insertAfter(element);
        }

    });

    $("#harvest-form").validate({
        rules: {
            pondname: {
                required: true,
            },
            species_type: {
                required: true,
            },
            weight: {
                required: true,
                number: true,
                min: 0
            },
            count: {
                required: true,
                number: true,
                min: 0
            },
            date: {
                required: true
            }
        },
        messages: {

            pondname: {
                required: disp_text("err_pname"),
            },
            species_type: {
                required: disp_text("err_stype"),
            },
            weight: {
                required: disp_text("err_weight"),
                number: disp_text("err_num"),
                min: disp_text("err_gt0")
            },
            count: {
                required: disp_text("err_count"),
                number: disp_text("err_num"),
                min: disp_text("err_gt0")
            },
            date: {
                required: disp_text("err_date"),
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "date")
                error.insertAfter(".datetimepicker-icon");
            else
                error.insertAfter(element);
        }

    });

    $("#mortality-form").validate({
        rules: {
            pondname: {
                required: true,
            },
            species_type: {
                required: true,
            },
            weight: {
                required: true,
                number: true,
                min: 0
            },
            count: {
                required: true,
                number: true,
                min: 0
            },
            reason: {
                required: true,
            },
            date: {
                required: true
            }
        },
        messages: {

            pondname: {
                required: disp_text("err_pname"),
            },
            species_type: {
                required: disp_text("err_stype"),
            },
            weight: {
                required: disp_text("err_weight"),
                number: disp_text("err_num"),
                min: disp_text("err_gt0")
            },
            count: {
                required: disp_text("err_count"),
                number: disp_text("err_num"),
                min: disp_text("err_gt0")
            },
            reason: {
                required: disp_text("err_reason"),
            },
            date: {
                required: "Please select date",
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "date")
                error.insertAfter(".datetimepicker-icon");
            else
                error.insertAfter(element);
        }

    });
	
	
    $("#feedstock-form").validate({
        rules: {
            feed_name: {
                required: true,
            },
            stocked_weight: {
                required: true,
                number: true,
                min: 0
            },
            date: {
                required: true
            }
        },
        messages: {

            feed_name: {
                required: disp_text("err_fname"),
            },
            stocked_weight: {
                required: disp_text("err_sweight"),
                number: disp_text("err_num"),
                min: disp_text("err_gt0")
            },
            date: {
                required: disp_text("err_date"),
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "date")
                error.insertAfter(".datetimepicker-icon");
            else
                error.insertAfter(element);
        }

    });	

    $("#feeddist-form").validate({
        rules: {
			pond_id: {
                required: true,
            },
            feed_stock_id: {
                required: true,
            },
            weight: {
                required: true,
                number: true,
                min: 0
            },
            date: {
                required: true
            }
        },
        messages: {
			 pond_id: {
                required: disp_text("err_pname"),
            },
            feed_stock_id: {
                required: disp_text("err_fname"),
            },
            weight: {
                required: disp_text("err_weight"),
                number: disp_text("err_num"),
                min: disp_text("err_gt0")
            },
            date: {
                required: disp_text("err_date"),
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "date")
                error.insertAfter(".datetimepicker-icon");
            else
                error.insertAfter(element);
        }

    });
	
    $('.cancel').on('click', function() {
        var form = $(this).closest("form");
        form[0].reset();
    });

    if ($('#datetimepicker').length > 0) {
        $('#datetimepicker').datetimepicker({
            format: 'L',
            keepOpen: false,
			maxDate: new Date()
        });
    }


    $(document).on("mousemove", function(event) {
        var dw = $(document).width() / 15;
        var dh = $(document).height() / 15;
        var x = event.pageX / dw;
        var y = event.pageY / dh;
        $('.eye-ball').css({
            width: x,
            height: y
        });
    });

    //data table
    //$('#bootstrap-data-table').DataTable();

    $('.delete-modal').on('click', function() {
        var rid = $(this).data('id');
        $('#delete_rid').val(rid);
    });
    if ($('.table.table-striped').length > 0) {
        $('.table.table-striped').floatThead({
            position: 'fixed'
        });
    }

    $('.modal').on('hide.bs.modal', function(e) {
        $('body').addClass('removepadding');
    });

    // edit ajax calls
    $('.edit-stock').on('click', function() {
        var rid = $(this).data('id');
        var card = $('#stockform-card .card-body');
        card.before('<div class="ajax-spinner" style="display:none;"><img src="' + baseUrl + '/assets/images/fish-loader.gif" /></div>');
        var height = card.height();
        var width = card.width();
        card.css('opacity', '0.1');
        //$('.ajax-spinner').css({'height': height,'width':width});
        $('.ajax-spinner').show();
        $.ajax({
            type: "POST",
            url: "stock/editStock",
            data: {
                'rid': rid
            },
        }).done(function(data) {
            $('#stockform-card').empty();
            $('#stockform-card').html(data);
            $('.ajax-spinner').hide();
            $('#stockform-card .card-body').css('opacity', '1');
        });
    });

    $('.edit-pond').on('click', function() {
        var rid = $(this).data('id');
        var card = $('#pondform-card .card-body');
        card.before('<div class="ajax-spinner" style="display:none;"><img src="' + baseUrl + '/assets/images/fish-loader.gif" /></div>');
        var height = card.height();
        var width = card.width();
        card.css('opacity', '0.1');
        //$('.ajax-spinner').css({'height': height,'width':width});
        $('.ajax-spinner').show();
        $.ajax({
            type: "POST",
            url: "pondlist/editPond",
            data: {
                'rid': rid
            },
        }).done(function(data) {
            $('#pondform-card').empty();
            $('#pondform-card').html(data);
            $('.ajax-spinner').hide();
            $('#pondform-card .card-body').css('opacity', '1');
        });
    });
	
	$('#feeddist-form #feed_stock_id').on('change', function(){
		var rid = $(this).val();
		console.log(rid);
		if(rid){
		  $.ajax({
			  type: "POST",
			  url: baseUrl+"feed/getcurrentFeed",
			  data: {
				  'rid': rid
			  },
		  }).done(function(data) {
			  var returnedData = JSON.parse(data);
			  $('#available_weight').val(parseInt(returnedData.result_set.current_weight));
			  $('.available_weight').text("Available weight is(" + returnedData.result_set.current_weight+" kg)");
			  if(returnedData.result_set.current_weight!="undefined")
			  checkFeedStock();
		  });		
		}else{
			$('.weight_exceed_err').hide();
			$('.available_weight').text('');
		}
	});
	
	$('#feeddist_weight').on('keyup', function(){
		var fWeight = $(this).val();
		checkFeedStock();
	});
	
	function checkFeedStock(){
		var fWeight = $('#feeddist_weight').val();
		var aWait = parseInt($('#available_weight').val());
		if(fWeight>aWait && aWait>0 && $('#feeddist-form #feed_stock_id').val()){
			$('#feeddist-form .btn-primary').attr('disabled',true);
			$('.weight_exceed_err').text("Weight is exceeding with available weight").show();
		}else{
			$('#feeddist-form .btn-primary').removeAttr('disabled');
			$('.weight_exceed_err').hide();
		}		
	}
	

    $('.carousel').carousel({});


    var ctx = document.getElementById("doughutChart");
    if (ctx) {

        //doughut chart
        $.ajax({
            type: "POST",
            url: "dashboard/getCurrentStock",
            dataType: "json",
        }).done(function(data) {
            var ponds = [];
            var fish_count = [];
            var species = [];
            var coloR = [];

            var dynamicColors = function() {
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * 255);
                var b = Math.floor(Math.random() * 255);
                return "rgb(" + r + "," + g + "," + b + ")";
            };
            for (var i in data.result_set) {
                ponds.push(data.result_set[i]._source.pondname + " ( " + data.result_set[i]._source.species_type + " )");
                fish_count.push(data.result_set[i]._source.count);
                species.push(data.result_set[i]._source.species_type);
                coloR.push(dynamicColors());
            }

            ctx.height = 150;
            var myChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    datasets: [{
                        data: fish_count,
                        backgroundColor: coloR,
                        hoverBackgroundColor: coloR

                    }],
                    labels: ponds
                },
                options: {
                    responsive: true
                }
            });
        });


    }
	

	$("#loadmore-activity").click(function(e){
		e.preventDefault();
		var page = $(this).data('val');
		getActivity(page);
	});
	
	var getActivity = function(page){
	 var card = $('#activity-feed-card .card-body');
        card.before('<div class="ajax-spinner" style="display:none;"><img src="' + baseUrl + '/assets/images/fish-loader.gif" /></div>');
		 $('.ajax-spinner').show();
	$.ajax({
		url:baseUrl+"Common/getActivities",
		type:'GET',
		data: {page:page}
		}).done(function(response){
			//console.log(response);
			if(response){
				$("#activity-feed").append(response);
				$('.ajax-spinner').hide();
				$('#nomore').remove();
				$('#loadmore-activity').data('val', ($('#loadmore-activity').data('val')+1));
				scroll();
			}else{
				$('.ajax-spinner').hide();
				$('#nomore').remove();
				$("#activity-feed").append('<div id="nomore" class="alert alert-danger col-md-6 offset-md-3" role="alert"> No more data found </div>');
			}
		});
	};
	var scroll  = function(){
	$('html, body').animate({
		scrollTop: $('#loadmore-activity').offset().top
	}, 1000);
	};
	
	//getActivity(1);

});