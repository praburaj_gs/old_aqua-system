jQuery(document).ready(function($) { 

var baseUrl = $('#base').val();

//view pondlist
	$('.view-modal').on('click', function(event) {
    var $row = $(this).closest("tr");    // Find the row
    var $tds = $row.find("td");
    $.each($tds, function(index,value) {
//        console.log($(this).text());				
		switch(index) {
			case 0:
				$("#pondnamemodel").html($(this).text());
			break;
			case 1:
				$("#pondlengthmodel").html($(this).text());
			break;
			case 2:
				$("#pondwidthmodel").html($(this).text());
			break;
			case 3:
				$("#ponddepthmodel").html($(this).text());
			break;
			case 4:
				$("#pondbuiltdatemodel").html($(this).text());
			break;
		} 
    });
	});
	
//view pondcleaning
	$('.view-modal').on('click', function(event) {
    var $row = $(this).closest("tr");    // Find the row
    var $tds = $row.find("td");
    $.each($tds, function(index,value) {
//        console.log($(this).text());				
		switch(index) {
			case 0:
				$("#pondnamemodel").html($(this).text());
			break;
			case 1:
				$("#pondcleanedbymodel").html($(this).text());
			break;
			case 2:
				$("#pondcleaneddatemodel").html($(this).text());
			break;
		} 
    });
	});
	
		//view distribution
	$('.view-modal').on('click', function(event) {
    var $row = $(this).closest("tr");    // Find the row
    var $tds = $row.find("td");
    $.each($tds, function(index,value) {
        console.log($(this).text());				
		switch(index) {
			case 0:
				$("#frompondmodel").html($(this).text());
			break;
			case 1:
				$("#topondmodel").html($(this).text());
			break;
			case 2:
				$("#speciestypemodel").html($(this).text());
			break;
			case 3:
				$("#countmodel").html($(this).text());
			break;
			case 4:
				$("#updatedbymodel").html($(this).text());
			break;
			case 5:
				$("#distributeddatemodel").html($(this).text());
			break;
		} 
    });
	});
	
		//view fishstock
	$('.view-modal').on('click', function(event) {
    var $row = $(this).closest("tr");    // Find the row
    var $tds = $row.find("td");
    $.each($tds, function(index,value) {
        console.log($(this).text());				
		switch(index) {
			case 0:
				$("#pondnamemodel").html($(this).text());
			break;
			case 1:
				$("#countmodel").html($(this).text());
			break;
			case 2:
				$("#speciestypemodel").html($(this).text());
			break;
			case 3:
				$("#weightmodel").html($(this).text());
			break;
			case 4:
				$("#fertilizermodel").html($(this).text());
			break;
			case 5:
				$("#watertypemodel").html($(this).text());
			break;
			case 6:
				$("#stockeddatemodel").html($(this).text());
			break;

		} 
    });
	});
	
		//view harvest
	$('.view-modal').on('click', function(event) {
    var $row = $(this).closest("tr");    // Find the row
    var $tds = $row.find("td");
    $.each($tds, function(index,value) {
//        console.log($(this).text());				
		switch(index) {
			case 0:
				$("#pondnamemodel").html($(this).text());
			break;
			case 1:
				$("#speciestypemodel").html($(this).text());
			break;
			case 2:
				$("#weightmodel").html($(this).text());
			break;
			case 3:
				$("#countmodel").html($(this).text());
			break;
			case 4:
				$("#harvesteddatemodel").html($(this).text());
			break;

		} 
    });
	});
	
			//view mortality
	$('.view-modal').on('click', function(event) {
    var $row = $(this).closest("tr");    // Find the row
    var $tds = $row.find("td");
    $.each($tds, function(index,value) {
//        console.log($(this).text());				
		switch(index) {
			case 0:
				$("#pondnamemodel").html($(this).text());
			break;
			case 1:
				$("#speciestypemodel").html($(this).text());
			break;
			case 2:
				$("#weightmodel").html($(this).text());
			break;
			case 3:
				$("#countmodel").html($(this).text());
			break;
			case 4:
				$("#reasonmodel").html($(this).text());
			break;
			case 5:
				$("#moralitydatemodel").html($(this).text());
			break;

		} 
    });
	});






//Validate pondlist
	$("#pondlist-form").validate({
		onkeyup: false,
	   	onclick: false,
	   	onfocusout: false,
		rules: {
			pondname: {
				required: true,
				chkduplicate: '{ "table_name": "ponds", "fld_name": "pondname"}'
			},
			width: {
				required: true,
				number: true,
				min:0
			},
			length: {
				required: true,
				number: true,
				min:0
			},
			depth: {
				required: true,
				number: true,
				min:0
			},
			built_date: {
				required: true,
			},
		},
		messages: {

			pondname: {
				required: disp_text("err_pname"),
				chkduplicate: disp_text("err_pname_exists")
			},
			width: {
				required: disp_text("err_pwidth"),
				number : disp_text("err_vwidth"),
				min : disp_text("err_gt0")
			},
			length: {
				required: disp_text("err_plength"),
				number : disp_text("err_vlength"),
				min : disp_text("err_gt0")
			},
			depth: {
				required: disp_text("err_pdepth"),
				number : disp_text("err_pdepth"),
				min : disp_text("err_gt0")
			},
			built_date: {
				required: disp_text("err_date"),
			}
	},
	errorPlacement: function(error, element) {
		
		if (element.attr("name") == "built_date" )
			error.insertAfter(".datetimepicker-icon");
		else
			error.insertAfter(element);
		}
	});
	
	$("#pondcleaning-form").validate({
		rules: {
			pondname: {
				required: true,
			},
			cleanedby: {
				required: true,
			},
			cleaned_date: {
				required: true,
			},
		},
		messages: {

			pondname: {
				required: disp_text("err_psname"),
			},
			cleanedby: {
				required: disp_text("err_cleanedby"),
			},
			cleaned_date: {
				required: disp_text("err_date"),
			},
	},
	errorPlacement: function(error, element) {
		
		if (element.attr("name") == "cleaned_date" )
			error.insertAfter(".datetimepicker-icon");
		else
			error.insertAfter(element);
		}

	});	
	
	//edit ajax calls
	$('.edit-pond').on('click', function(){
  		var rid = $(this).data('id');
		var card = $('#pondcleaningform-card .card-body');
		card.before('<div class="ajax-spinner" style="display:none;"><img src="'+baseUrl+'/assets/images/fish-loader.gif" /></div>');
		var length=card.height();
		var width=card.width();
		card.css('opacity','0.1');		
		//$('.ajax-spinner').css({'height': height,'width':width});
		$('.ajax-spinner').show();
		$.ajax({
			type:"POST",
			url:"pondcleaning/editPondcleaning",
			data: { 'rid' : rid },
		  }).done(function(data) {
			 $('#pondcleaningform-card').empty();
			 $('#pondcleaningform-card').html(data);
			 $('.ajax-spinner').hide();
			 $('#pondcleaningform-card .card-body').css('opacity','1');
		  });
	});


	
});

